/*  test-glimmer-widget.c
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <libgnome-file-selector/gnome-file-selector.h>

static void exit_cb(GtkWidget *w, gpointer data);
static void open_cb(GtkWidget *w, gpointer data);
static void save_cb(GtkWidget *w, gpointer data);
static void dir_multi_cb (GtkWidget *w, gpointer data);
static void dir_single_cb (GtkWidget *w, gpointer data);
static void delete_event_cb (GtkWidget *w, GdkEvent *event, gpointer data);
static void mutate_file_to_dir(GtkWidget *w, gpointer data);
static void mutate_dir_to_file(GtkWidget *w, gpointer data);

static GtkWidget *mutate_dir = NULL;
static GtkWidget *mutate_file = NULL;

static void 
exit_cb (GtkWidget *w, gpointer data)
{
    gtk_main_quit ();
}

static void 
open_cb (GtkWidget *w, gpointer data)
{
    GtkWidget *window;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    w = gnome_file_selector_new("test-id", GNOME_FILE_SELECTOR_FILE_MULTIPLE, GTK_STOCK_OPEN);
    gnome_file_selector_set_mime_types(GNOME_FILE_SELECTOR(w), "All Files:*/*|All Images:image/*|C Source Files:text/x-c");
    g_signal_connect(G_OBJECT(w), "delete_event", (GCallback)gtk_widget_destroy, NULL);
    gtk_container_add(GTK_CONTAINER(window), w);
    gtk_widget_show(w);
    gtk_widget_show(window);
}

static void 
save_cb (GtkWidget *w, gpointer data)
{
    GtkWidget *window;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    w = gnome_file_selector_new("test-id", GNOME_FILE_SELECTOR_FILE_SINGLE, GTK_STOCK_SAVE);
    gnome_file_selector_set_mime_types(GNOME_FILE_SELECTOR(w), "All Files:*/*|All Images:image/*|C Source Files:text/x-c");
    g_signal_connect(G_OBJECT(w), "delete_event", (GCallback)gtk_widget_destroy, NULL);
    g_signal_connect(G_OBJECT(w), "cancel", (GCallback)mutate_file_to_dir, NULL);
    gtk_container_add(GTK_CONTAINER(window), w);
    gtk_widget_show(w);
    gtk_widget_show(window);
}

static void
mutate_file_to_dir(GtkWidget *w, gpointer data)
{
    if(gnome_file_selector_get_selector_type(GNOME_FILE_SELECTOR(w)) == GNOME_FILE_SELECTOR_DIR_SINGLE)
        gnome_file_selector_set_selector_type(GNOME_FILE_SELECTOR(w), GNOME_FILE_SELECTOR_FILE_SINGLE);
    else
        gnome_file_selector_set_selector_type(GNOME_FILE_SELECTOR(w), GNOME_FILE_SELECTOR_DIR_SINGLE);
}

static void 
dir_multi_cb (GtkWidget *w, gpointer data)
{
    GtkWidget *window;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    w = gnome_file_selector_new("test-id", GNOME_FILE_SELECTOR_DIR_MULTIPLE, GTK_STOCK_OPEN);
    g_signal_connect(G_OBJECT(w), "delete_event", (GCallback)gtk_widget_destroy, NULL);
    gtk_container_add(GTK_CONTAINER(window), w);
    gtk_widget_show(w);
    gtk_widget_show(window);
}

static void 
dir_single_cb (GtkWidget *w, gpointer data)
{
    GtkWidget *window;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    w = gnome_file_selector_new("test-id", GNOME_FILE_SELECTOR_DIR_SINGLE, GTK_STOCK_SAVE);
    g_signal_connect(G_OBJECT(w), "delete_event", (GCallback)gtk_widget_destroy, NULL);
    g_signal_connect(G_OBJECT(w), "cancel", (GCallback)mutate_file_to_dir, NULL);
    gtk_container_add(GTK_CONTAINER(window), w);
    gtk_widget_show(w);
    gtk_widget_show(window);
}

static void
mutate_dir_to_file(GtkWidget *w, gpointer data)
{
    if(gnome_file_selector_get_selector_type(GNOME_FILE_SELECTOR(w)) == GNOME_FILE_SELECTOR_DIR_SINGLE)
        gnome_file_selector_set_selector_type(GNOME_FILE_SELECTOR(w), GNOME_FILE_SELECTOR_FILE_SINGLE);
    else
        gnome_file_selector_set_selector_type(GNOME_FILE_SELECTOR(w), GNOME_FILE_SELECTOR_DIR_SINGLE);
}

static gint
idle_hands_are_the_devils_playground(gpointer data)
{
    GtkWidget *win;
    GtkWidget *widget;
    GtkWidget *button;

    win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_container_set_border_width(GTK_CONTAINER(win), 5);
    g_signal_connect(G_OBJECT(win), "delete_event", (GCallback)delete_event_cb, (gpointer)win);
    widget = gtk_vbox_new(TRUE, 5);
    gtk_container_add(GTK_CONTAINER(win), widget);
    button = gtk_button_new_from_stock(GTK_STOCK_OPEN);
    g_signal_connect(G_OBJECT(button), "clicked", (GCallback)open_cb, (gpointer)win);
    gtk_container_add(GTK_CONTAINER(widget), button);
    button = gtk_button_new_from_stock(GTK_STOCK_SAVE);
    g_signal_connect(G_OBJECT(button), "clicked", (GCallback)save_cb, (gpointer)win);
    gtk_container_add(GTK_CONTAINER(widget), button);
    button = gtk_button_new_with_label("Multiple selection directory (Open)");
    g_signal_connect(G_OBJECT(button), "clicked", (GCallback)dir_multi_cb, (gpointer)win);
    gtk_container_add(GTK_CONTAINER(widget), button);
    button = gtk_button_new_with_label("Single selection directory (Save)");
    g_signal_connect(G_OBJECT(button), "clicked", (GCallback)dir_single_cb, (gpointer)win);
    gtk_container_add(GTK_CONTAINER(widget), button);
    gtk_widget_show_all (win);
    return(0);
}

static void
delete_event_cb (GtkWidget *w, GdkEvent *event, gpointer data)
{
    exit_cb (w, data);
}

int
main(int argc, char *argv[])
{
    gtk_init(&argc, &argv);
	gtk_idle_add((GtkFunction)idle_hands_are_the_devils_playground, NULL);
    gtk_main();
    return(0);
}
