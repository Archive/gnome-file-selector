/*  gnome-file-selector.c
 *
 *  Gnome-Enabled VFS Based File Selector based on GTK+ 2.0
 *  Copyright (C) 2001 Chris Phelps <chicane@reninet.com
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <glib.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libbonobo.h>
#include <libbonoboui.h>
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>

#ifdef _GNU_SOURCE
#include <fnmatch.h>
#else
#define _GNU_SOURCE
#include <fnmatch.h>
/* On Solaris 2.7 we need to undef this again.  */
#undef _GNU_SOURCE
#endif

#include "gnome-file-selector.h"
#include "gnome-file-selector-private.h"
#include "gnome-file-selector-gconf.h"
#include "gnome-file-selector-history.h"

#include "pixmaps/file-selector-dots.xpm"
#include "pixmaps/file-selector-no-dots.xpm"
#include "pixmaps/file-selector-details.xpm"
#include "pixmaps/file-selector-no-details.xpm"
#include "pixmaps/file-selector-folder.xpm"
#include "pixmaps/file-selector-file.xpm"

/* function declarations */
static void gnome_file_selector_class_init (GnomeFileSelectorClass *klass);
static void gnome_file_selector_init (GnomeFileSelector *file_selector);
static void gnome_file_selector_finalize (GObject *object);
static void gnome_file_selector_set_property (GObject *object,
					      guint prop_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gnome_file_selector_get_property (GObject *object,
					      guint prop_id,
					      GValue *value,
					      GParamSpec *pspec);

static void gnome_file_selector_set_show_dots (GnomeFileSelector    *file_selector,
					       gboolean              show_dots);
static gboolean gnome_file_selector_show_dots (GnomeFileSelector    *file_selector);
static void gnome_file_selector_set_show_details (GnomeFileSelector    *file_selector,
						  gboolean              show_details);
static gboolean gnome_file_selector_show_details (GnomeFileSelector    *file_selector);

static void directory_dnd_drag_begin_cb (GtkWidget *widget,
					 GdkDragContext *context,
					 GnomeFileSelector *file_selector);
static void file_dnd_drag_begin_cb (GtkWidget *widget,
				    GdkDragContext *context,
				    GnomeFileSelector *file_selector);
static void directory_dnd_set_data_cb (GtkWidget *widget,
				       GdkDragContext *context,
				       GtkSelectionData *selection_data,
				       guint info, guint time,
				       GnomeFileSelector *file_selector);
static void file_dnd_set_data_cb (GtkWidget *widget,
				  GdkDragContext *context,
				  GtkSelectionData *selection_data,
				  guint info, guint time,
				  GnomeFileSelector *file_selector);
static GtkWidget *init_tree_store (GnomeFileSelector *file_selector,
				   GtkWidget *scrolled_parent,
				   gboolean file_list);
static void modify_tree_columns (GnomeFileSelector *file_selector,
				 gboolean file_list);
static void directory_tree_selection_changed (GtkTreeSelection *selection,
					      GnomeFileSelector *file_selector);
static void directory_tree_row_activate (GtkTreeView *tree_view,
					 GtkTreePath *path,
					 GtkTreeViewColumn *column,
					 GnomeFileSelector *file_selector);
static void file_tree_row_activate (GtkTreeView *tree_view,
				    GtkTreePath *path,
				    GtkTreeViewColumn *column,
				    GnomeFileSelector *file_selector);
static void file_tree_selection_changed (GtkTreeSelection *selection,
					 GnomeFileSelector *file_selector);
static void action_button_pressed(GtkWidget *button, GnomeFileSelector *file_selector);
static void cancel_button_pressed(GtkWidget *button, GnomeFileSelector *file_selector);

static void hash_remove_func (gchar *key, gpointer data, gpointer d);

static gint check_complete (GtkWidget *widget, GdkEventKey *event, GnomeFileSelector *file_selector);
static void gnome_file_selector_try_complete (GnomeFileSelector *file_selector,
					      const gchar *complete);

static gboolean gnome_file_selector_get_listing (GnomeFileSelector *file_selector);
static void insert_node (GnomeFileSelector *file_selector, GnomeVFSFileInfo *finfo);
static GdkPixbuf *create_pixbuf_scaled (const gchar *icon_filename);
static void directory_load_cb (GnomeVFSAsyncHandle *handle,
			       GnomeVFSResult result, GList *list,
			       guint entries_read, gpointer callback_data);
static void tree_insert_node_alphabetically (GtkTreeView *tree,
					     GdkPixbuf *pixbuf,
					     gchar *name,
					     gchar *size_str,
					     gchar *perms_str,
					     gboolean details);
static gchar *get_filter (GnomeFileSelector *file_selector);
static gboolean filter_match (GnomeFileSelector *file_selector, const gchar *filename);

static void selection_list_func (GtkTreeModel *model, GtkTreePath *path,
				 GtkTreeIter *iter, gpointer data);
static void refresh_listing (GtkWidget *widget,
			     GnomeFileSelector *file_selector);
static void history_go_back (GtkWidget *widget,
			     GnomeFileSelector *file_selector);
static void history_go_forward (GtkWidget *widget,
			        GnomeFileSelector *file_selector);
static void goto_parent (GtkWidget *widget,
			 GnomeFileSelector *file_selector);
static void check_action_button_cb (GtkWidget *widget,
				    GnomeFileSelector *file_selector);
static gchar *get_parent_dir (const gchar *path);
static gchar *build_full_path (const gchar *path, const gchar *selection);
static GnomeVFSURI *build_uri_path (const gchar *path,
				    const gchar *selection);
static void check_goto_dir (GtkWidget *widget,
			    GnomeFileSelector *file_selector);
static void check_filter_changed (GtkWidget *widget,
				  GnomeFileSelector *file_selector);
static void check_filter_activate (GtkWidget *widget,
				   GnomeFileSelector *file_selector);
static void check_goto (GtkWidget *widget,
		        GnomeFileSelector *file_selector);
static void create_directory (GtkWidget *widget,
			      GnomeFileSelector *file_selector);
static void delete_file (GtkWidget *widget,
			 GnomeFileSelector *file_selector);
static void rename_file (GtkWidget *widget,
			 GnomeFileSelector *file_selector);
static void toggle_show_dots (GtkToggleButton *button,
			      GnomeFileSelector *file_selector);
static void toggle_show_details (GtkToggleButton *button,
				 GnomeFileSelector *file_selector);

static void tree_view_set_selection_type (GtkTreeView *tree,
					  GtkSelectionMode type);

static gboolean check_dir_exists (const gchar *path);
static gboolean check_file_exists (const gchar *filename);
static gboolean check_path_local (const gchar *path);
static gchar *build_rwx_values (GnomeVFSFilePermissions permissions);

static void register_selector_for_notification (GnomeFileSelector *file_selector);
static void nautilus_theme_changed (GConfClient *client, guint cnxn_id,
				    GConfEntry *entry,
				    GnomeFileSelector *file_selector);
static void file_selector_setup_default_icons (GnomeFileSelector *file_selector);
static void set_icons_from_nautilus_theme (GnomeFileSelector * file_selector,
					   const gchar *theme);
static void inplace_replace (gchar *string, gchar replace, gchar with);

/* end function declarations */

enum {
	ACTION,
	CANCEL,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_SHOW_DOTS,
	PROP_SHOW_DETAILS
};

enum {
	COLUMN_PIXBUF,
	COLUMN_NAME,
	COLUMN_SIZE,
	COLUMN_PERMISSIONS,
	NUM_COLUMNS_DETAILS
};

#define NUM_COLUMNS_NORMAL 2

enum {
	TEXT_URI_LIST,
};

static GtkTargetEntry treeview_drag_types[] = {
	{"text/uri-list", 0, TEXT_URI_LIST}
};
static gint n_treeview_drag_types = 1;

static GObjectClass *parent_class = NULL;
static guint selector_signals[LAST_SIGNAL] = { 0 };

GType gnome_file_selector_get_type ()
{
	static GType our_type = 0;

	if (our_type == 0) {
		static const GTypeInfo our_info = {
			sizeof (GnomeFileSelectorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_file_selector_class_init,
			NULL,	/* class_finalize */
			NULL,	/* class_data */
			sizeof (GnomeFileSelector),
			0,	/* n_preallocs */
			(GInstanceInitFunc) gnome_file_selector_init
		};

		our_type = g_type_register_static (GTK_TYPE_VBOX,
						  "GnomeFileSelector",
						   &our_info, 0);
	}
	return our_type;
}

static void
gnome_file_selector_class_init (GnomeFileSelectorClass *klass)
{
	GObjectClass *gobject_class;
	GtkObjectClass *object_class;

	parent_class = g_type_class_peek_parent (klass);
	gobject_class = G_OBJECT_CLASS (klass);
	object_class = GTK_OBJECT_CLASS (klass);

	gobject_class->set_property = gnome_file_selector_set_property;
	gobject_class->get_property = gnome_file_selector_get_property;
	gobject_class->finalize = gnome_file_selector_finalize;

	g_object_class_install_property (gobject_class, PROP_SHOW_DOTS,
					 g_param_spec_boolean ("show_dots",
							     _("Show files beginning with '.'"),
							     _("Whether the files beginning with '.' should be shown"),
							       FALSE, G_PARAM_READABLE | G_PARAM_WRITABLE));

	g_object_class_install_property (gobject_class, PROP_SHOW_DETAILS,
					 g_param_spec_boolean ("show_details",
							     _("Show detailed information of files"),
							     _("Whether detailed file information should be shown"),
							       FALSE, G_PARAM_READABLE | G_PARAM_WRITABLE));

	selector_signals[ACTION] =
		g_signal_new ("action",
			       GTK_CLASS_TYPE (gobject_class),
			       G_SIGNAL_RUN_FIRST,
			       GTK_SIGNAL_OFFSET (GnomeFileSelectorClass, action),
			       NULL,
			       NULL,
			       g_cclosure_marshal_VOID__VOID,
			       G_TYPE_NONE,
			       0, NULL);
	selector_signals[CANCEL] =
		g_signal_new ("cancel",
			       GTK_CLASS_TYPE (gobject_class),
			       G_SIGNAL_RUN_FIRST,
			       GTK_SIGNAL_OFFSET (GnomeFileSelectorClass, cancel),
			       NULL,
			       NULL,
			       g_cclosure_marshal_VOID__VOID,
			       G_TYPE_NONE,
			       0, NULL);

	gconf_extensions_client_setup ();

	if (!gnome_vfs_initialized ())
		gnome_vfs_init ();
}

static void
gnome_file_selector_init (GnomeFileSelector *file_selector)
{
	file_selector->priv = g_new0(GnomeFileSelectorPrivate, 1);

	file_selector->priv->history_id = NULL;
	file_selector->priv->show_dots = TRUE;
	file_selector->priv->show_details = FALSE;
	file_selector->priv->history_max = 10;
	file_selector->priv->history_combo = NULL;
	file_selector->priv->directory_list = NULL;
	file_selector->priv->file_list = NULL;
	file_selector->priv->folder_pixbuf = NULL;
	file_selector->priv->file_pixbuf = NULL;
	file_selector->priv->history = NULL;
	file_selector->priv->path = NULL;
	file_selector->priv->history_position = -1;
	file_selector->priv->pixmaps = NULL;
	file_selector->priv->load_handle = NULL;
	file_selector->priv->new_hash = NULL;
}

GtkWidget *
gnome_file_selector_construct (GnomeFileSelector    *file_selector,
			       const gchar          *history_id,
			       const gchar          *path,
			       GnomeFileSelectorType type,
			       const gchar          *stock_id)
{
	GtkWidget *util_box;
	GtkWidget *toolbar;
	GdkPixbuf *pixbuf;
	GtkWidget *image;
	GtkWidget *paned;
	GtkWidget *scrolled_window1;
	GtkWidget *scrolled_window2;
	GtkWidget *label;
	GtkWidget *hsep;
	GtkWidget *parent_button;
	GtkWidget *refresh_button;

	if (history_id) {
		file_selector->priv->history_id = g_strdup (history_id);
		inplace_replace (file_selector->priv->history_id, ' ', '-');
		gnome_file_selector_load_history (file_selector);
	}

	gtk_container_set_border_width (GTK_CONTAINER (file_selector), 5);

	toolbar = gtk_toolbar_new ();
	gtk_toolbar_set_orientation (GTK_TOOLBAR (toolbar),
				    GTK_ORIENTATION_HORIZONTAL);
	gtk_toolbar_set_style (GTK_TOOLBAR (toolbar), GTK_TOOLBAR_ICONS);
	gtk_toolbar_set_icon_size (GTK_TOOLBAR (toolbar),
				   GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_box_pack_start (GTK_BOX (file_selector), toolbar, FALSE, TRUE, 0);
	gtk_widget_show (toolbar);

	file_selector->priv->back_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar),
				      GTK_STOCK_GO_BACK,
				      _("Go back to a previous directory"),
				      0, GTK_SIGNAL_FUNC (history_go_back),
				      file_selector, -1);
	file_selector->priv->forward_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar),
				      GTK_STOCK_GO_FORWARD,
				      _("Go to the next directory"), 0,
				      GTK_SIGNAL_FUNC (history_go_forward),
				      file_selector, -1);
	parent_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_GO_UP,
				      _("Go to parent directory"), 0,
				      GTK_SIGNAL_FUNC (goto_parent),
				      file_selector, -1);
	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
	refresh_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar),
				      GTK_STOCK_REFRESH,
				      _("Refresh listing"), 0,
				      GTK_SIGNAL_FUNC (refresh_listing),
				      file_selector, -1);
	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));
	file_selector->priv->create_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_OPEN,
				      _("Create directory"), 0,
				      GTK_SIGNAL_FUNC (create_directory),
				      file_selector, -1);
	file_selector->priv->delete_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar), GTK_STOCK_CLOSE,
				      _("Delete file"), 0,
				      GTK_SIGNAL_FUNC (delete_file),
				      file_selector, -1);
	file_selector->priv->rename_button =
	    gtk_toolbar_insert_stock (GTK_TOOLBAR (toolbar),
				      GTK_STOCK_CONVERT, _ ("Rename file"),
				      0, GTK_SIGNAL_FUNC (rename_file),
				      file_selector, -1);
	gtk_toolbar_append_space (GTK_TOOLBAR (toolbar));

	pixbuf =
	    gdk_pixbuf_new_from_xpm_data ((const gchar **) (file_selector->priv->
							   show_dots ?
							   file_selector_dots_xpm
							   :
							   file_selector_no_dots_xpm));
	image = gtk_image_new_from_pixbuf (pixbuf);
	file_selector->priv->dots_toggle = gtk_toggle_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (file_selector->priv->dots_toggle),
				      file_selector->priv->show_dots);
	gtk_container_add (GTK_CONTAINER (file_selector->priv->dots_toggle), image);
	gtk_widget_show_all (file_selector->priv->dots_toggle);
	gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar),
				   file_selector->priv->dots_toggle,
				    _("Hidden files"),
				    _("Toggle visibility of hidden files"));
	g_signal_connect (G_OBJECT (file_selector->priv->dots_toggle),
			 "toggled", G_CALLBACK (toggle_show_dots),
			  file_selector);

	pixbuf =
	    gdk_pixbuf_new_from_xpm_data ((const gchar **) (file_selector->priv->
							   show_details ?
							   file_selector_details_xpm
							   :
							   file_selector_no_details_xpm));
	image = gtk_image_new_from_pixbuf (pixbuf);
	file_selector->priv->details_toggle = gtk_toggle_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
				     (file_selector->priv->details_toggle),
				      file_selector->priv->show_details);
	gtk_container_add (GTK_CONTAINER (file_selector->priv->details_toggle), image);
	gtk_widget_show_all (file_selector->priv->details_toggle);
	gtk_toolbar_append_widget (GTK_TOOLBAR (toolbar),
				   file_selector->priv->details_toggle,
				   _("File Details"),
				   _("Toggle visibility of file details"));
	g_signal_connect (G_OBJECT (file_selector->priv->details_toggle),
			 "toggled", G_CALLBACK (toggle_show_details),
			  file_selector);

	util_box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (file_selector), util_box, FALSE, FALSE, 0);
	gtk_widget_show (util_box);

	label = gtk_label_new (_("Directory: "));
	gtk_box_pack_start (GTK_BOX (util_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	file_selector->priv->history_combo = gtk_combo_new ();
	gtk_combo_disable_activate (GTK_COMBO (file_selector->priv->history_combo));
	gtk_box_pack_start (GTK_BOX (util_box), file_selector->priv->history_combo,
			    TRUE, TRUE, 0);
	file_selector->priv->history_entry = GTK_COMBO (file_selector->priv->history_combo)->entry;
	if (file_selector->priv->history)
		gtk_combo_set_popdown_strings (GTK_COMBO (file_selector->priv->history_combo),
					       file_selector->priv->history);
	gtk_widget_show (file_selector->priv->history_combo);

	util_box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (file_selector), util_box, TRUE, TRUE, 0);
	gtk_widget_show (util_box);

	paned = gtk_hpaned_new ();
	gtk_box_pack_start (GTK_BOX (util_box), paned, TRUE, TRUE, 0);
	gtk_widget_show (paned);

	scrolled_window1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window1),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window1),
				        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_set_size_request (scrolled_window1, 200, 250);
	gtk_widget_show (scrolled_window1);
	gtk_paned_add1 (GTK_PANED (paned), scrolled_window1);
	file_selector->priv->directory_list =
	    init_tree_store (file_selector, scrolled_window1, FALSE);
	g_signal_connect (G_OBJECT (file_selector->priv->directory_list),
			 "row_activated",
			 (GCallback) directory_tree_row_activate,
			  file_selector);
	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (
			  GTK_TREE_VIEW (file_selector->priv->directory_list))),
			 "changed",
			 (GCallback) directory_tree_selection_changed,
			  file_selector);

	/* Hook up Directory DnD support */
	gtk_drag_source_set (GTK_WIDGET (file_selector->priv->directory_list),
			     GDK_BUTTON1_MASK, treeview_drag_types,
			     n_treeview_drag_types,
			     GDK_ACTION_COPY | GDK_ACTION_LINK |
			     GDK_ACTION_ASK);
	g_signal_connect (G_OBJECT (file_selector->priv->directory_list),
			 "drag_data_get",
			  G_CALLBACK (directory_dnd_set_data_cb),
			  file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->directory_list),
			 "drag_begin",
			  G_CALLBACK (directory_dnd_drag_begin_cb),
			  file_selector);

	scrolled_window2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window2),
					     GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window2),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_set_size_request (scrolled_window2, 300, 250);
	gtk_widget_show (scrolled_window2);
	gtk_paned_add2 (GTK_PANED (paned), scrolled_window2);

	file_selector->priv->file_list =
	    init_tree_store (file_selector, scrolled_window2, TRUE);
	g_signal_connect (G_OBJECT (file_selector->priv->file_list),
			 "row_activated",
			 (GCallback) file_tree_row_activate,
			  file_selector);
	g_signal_connect (G_OBJECT (gtk_tree_view_get_selection (GTK_TREE_VIEW (file_selector->priv->file_list))),
			 "changed",
			 (GCallback) file_tree_selection_changed,
			  file_selector);

	/* Hook up File DnD support */
	gtk_drag_source_set (GTK_WIDGET (file_selector->priv->file_list),
			     GDK_BUTTON1_MASK, treeview_drag_types,
			     n_treeview_drag_types,
			     GDK_ACTION_COPY | GDK_ACTION_LINK |
			     GDK_ACTION_ASK);
	g_signal_connect (G_OBJECT (file_selector->priv->file_list),
			 "drag_data_get", G_CALLBACK (file_dnd_set_data_cb),
			  file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->file_list), "drag_begin",
			  G_CALLBACK (file_dnd_drag_begin_cb),
			  file_selector);

	util_box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (file_selector), util_box, FALSE, FALSE, 0);
	gtk_widget_show (util_box);

	label = gtk_label_new (_("Selection: "));
	gtk_box_pack_start (GTK_BOX (util_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	file_selector->priv->selection_entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (util_box),
			    file_selector->priv->selection_entry, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (file_selector->priv->selection_entry),
			 "changed", G_CALLBACK (check_action_button_cb),
			  file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->selection_entry),
			 "key_press_event", G_CALLBACK (check_complete),
			  file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->selection_entry),
			 "activate", G_CALLBACK (check_goto),
			  file_selector);
	gtk_widget_show (file_selector->priv->selection_entry);

	util_box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (file_selector), util_box, FALSE, FALSE, 0);
	gtk_widget_show (util_box);

	label = gtk_label_new (_("Filter: "));
	gtk_box_pack_start (GTK_BOX (util_box), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	file_selector->priv->filter_combo = gtk_combo_new ();
	gtk_combo_disable_activate (GTK_COMBO (file_selector->priv->filter_combo));
	gtk_box_pack_start (GTK_BOX (util_box), file_selector->priv->filter_combo,
			    TRUE, TRUE, 0);
	file_selector->priv->filter_entry = GTK_COMBO (file_selector->priv->filter_combo)->entry;
	g_signal_connect (G_OBJECT (file_selector->priv->filter_entry), "changed",
			 (GCallback) check_filter_changed, file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->filter_entry), "activate",
			 (GCallback) check_filter_activate, file_selector);
	gtk_widget_show (file_selector->priv->filter_combo);

	hsep = gtk_hseparator_new ();
	gtk_box_pack_start (GTK_BOX (file_selector), hsep, FALSE, TRUE, 0);
	gtk_widget_show (hsep);

	/* Create action area */
	file_selector->priv->action_area = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (file_selector->priv->action_area),
				   GTK_BUTTONBOX_END);  
	gtk_box_set_spacing (GTK_BOX (file_selector->priv->action_area), 5);
	gtk_box_pack_end (GTK_BOX (file_selector), file_selector->priv->action_area,
			  FALSE, TRUE, 0);
	gtk_widget_show (file_selector->priv->action_area);

	file_selector->priv->cancel_button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
	g_signal_connect (file_selector->priv->cancel_button,
			 "clicked", G_CALLBACK (cancel_button_pressed), file_selector);
	GTK_WIDGET_SET_FLAGS (file_selector->priv->cancel_button,
			      GTK_CAN_DEFAULT);
	gtk_box_pack_end (GTK_BOX (file_selector->priv->action_area),
			  file_selector->priv->cancel_button,
			  FALSE, FALSE, 0);
	gtk_widget_show (file_selector->priv->cancel_button);

	file_selector->priv->action_button = gtk_button_new_from_stock (stock_id);
	g_signal_connect (file_selector->priv->action_button,
			 "clicked", G_CALLBACK (action_button_pressed), file_selector);
	GTK_WIDGET_SET_FLAGS (file_selector->priv->action_button,
			      GTK_CAN_DEFAULT);
	gtk_box_pack_end (GTK_BOX (file_selector->priv->action_area),
			  file_selector->priv->action_button,
			  FALSE, FALSE, 0);
	gtk_widget_set_sensitive (file_selector->priv->action_button, FALSE);
	gtk_widget_show (file_selector->priv->action_button);

	if (type == GNOME_FILE_SELECTOR_FILE_MULTIPLE ||
	    type == GNOME_FILE_SELECTOR_FILE_SINGLE)
		file_selector->priv->type = GNOME_FILE_SELECTOR_DIR_SINGLE;

	else if (type == GNOME_FILE_SELECTOR_DIR_MULTIPLE ||
		 type == GNOME_FILE_SELECTOR_DIR_SINGLE)
		file_selector->priv->type = GNOME_FILE_SELECTOR_FILE_SINGLE;

	gnome_file_selector_set_selector_type (file_selector, type);

	file_selector_setup_default_icons (file_selector);
	register_selector_for_notification (file_selector);

	file_selector->priv->pixmaps = g_hash_table_new (g_str_hash, g_str_equal);

	g_signal_connect (G_OBJECT (file_selector->priv->history_entry),
			 "changed", (GCallback) check_action_button_cb,
			  file_selector);
	g_signal_connect (G_OBJECT (file_selector->priv->history_entry),
			 "activate", (GCallback) check_goto_dir,
			  file_selector);

	if (!file_selector->priv->history && path) {
		gnome_file_selector_set_directory (file_selector, path);
	} else if (file_selector->priv->history) {
		file_selector->priv->path =
		    g_strdup (file_selector->priv->history->data);
		gnome_file_selector_get_listing (file_selector);
	} else {
		gnome_file_selector_set_directory (file_selector, g_get_home_dir ());
	}
	gtk_widget_grab_focus (file_selector->priv->selection_entry);
	return GTK_WIDGET (file_selector);
}

GtkWidget *
gnome_file_selector_new (const gchar	  *history_id,
			 GnomeFileSelectorType type,
			 const gchar	  *stock_id)
{
	return gnome_file_selector_new_with_path (
		history_id, NULL, type, stock_id);
}

GtkWidget *
gnome_file_selector_new_with_path (const gchar	  *history_id,
				   const gchar	  *path,
				   GnomeFileSelectorType type,
				   const gchar	  *stock_id)
{
	GnomeFileSelector *file_selector;

	file_selector = g_object_new (GNOME_TYPE_FILE_SELECTOR, "spacing", 5, NULL);

	return gnome_file_selector_construct (
		file_selector, history_id, path, type, stock_id);
}

static void
gnome_file_selector_finalize (GObject *object)
{
	GnomeFileSelector *file_selector;
	GList *iter;

	g_return_if_fail (GNOME_IS_FILE_SELECTOR (object));

	g_print ("destroying the GnomeFileSelector widget\n");
	file_selector = GNOME_FILE_SELECTOR (object);

	if (file_selector->priv->load_handle) {
		g_print ("Killing widget causes vfs cancel...\n");
		gnome_vfs_async_cancel (file_selector->priv->load_handle);
	}

	g_object_unref (G_OBJECT (file_selector->priv->file_pixbuf));
	g_object_unref (G_OBJECT (file_selector->priv->folder_pixbuf));

	if (file_selector->priv->pixmaps) {
		g_hash_table_foreach_remove (file_selector->priv->pixmaps,
					    (GHRFunc) hash_remove_func,
					    NULL);
		g_hash_table_destroy (file_selector->priv->pixmaps);
	}

	if (file_selector->priv->gconf_connection) {
		GConfClient *client;

		client = gconf_extensions_client_get ();
		if (client)
			gconf_client_notify_remove (client,
						    file_selector->priv->
						    gconf_connection);
	}

	gnome_file_selector_save_history (file_selector);

	g_free (file_selector->priv->history_id);
	g_free (file_selector->priv->path);

	for (iter = file_selector->priv->history; iter; iter = iter->next)
		g_free (iter->data);
	g_list_free (file_selector->priv->history);

	for(iter = file_selector->priv->mime_types; iter; iter = iter->next)
		g_free(iter->data);
	g_list_free(file_selector->priv->mime_types);

	g_free(file_selector->priv);
	parent_class->finalize (object);
}

static void
gnome_file_selector_set_property (GObject *object,
				  guint prop_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
	GnomeFileSelector *file_selector = GNOME_FILE_SELECTOR (object);

	switch (prop_id) {
		case PROP_SHOW_DOTS:
			gnome_file_selector_set_show_dots (file_selector,
							   g_value_get_boolean (value));
			break;
		case PROP_SHOW_DETAILS:
			gnome_file_selector_set_show_details (file_selector,
							      g_value_get_boolean (value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gnome_file_selector_get_property (GObject *object,
				  guint prop_id,
				  GValue *value,
				  GParamSpec *pspec)
{
	GnomeFileSelector *file_selector = GNOME_FILE_SELECTOR (object);

	switch (prop_id) {
		case PROP_SHOW_DOTS:
			g_value_set_boolean (value,
					     gnome_file_selector_show_dots (file_selector));
			break;
		case PROP_SHOW_DETAILS:
			g_value_set_boolean (value,
					     gnome_file_selector_show_details (file_selector));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
			break;
	}
}

static void
gnome_file_selector_set_show_dots (GnomeFileSelector *file_selector,
				   gboolean	   show_dots)
{
	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	if (file_selector->priv->show_dots != show_dots) {
		file_selector->priv->show_dots = show_dots;
		gtk_toggle_button_set_active (
			GTK_TOGGLE_BUTTON (file_selector->priv->dots_toggle),
			file_selector->priv->show_dots);
		gnome_file_selector_get_listing (file_selector);
	}
}

static gboolean
gnome_file_selector_show_dots (GnomeFileSelector *file_selector)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), FALSE);

	return file_selector->priv->show_dots;
}

static void
gnome_file_selector_set_show_details (GnomeFileSelector *file_selector,
				     gboolean show_details)
{
	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	if (file_selector->priv->show_details != show_details) {
		file_selector->priv->show_details = show_details;
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					     (file_selector->priv->
					      details_toggle),
					      file_selector->priv->show_details);
		modify_tree_columns (file_selector, FALSE);
		modify_tree_columns (file_selector, TRUE);
	}
}

static gboolean
gnome_file_selector_show_details (GnomeFileSelector *file_selector)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), FALSE);

	return file_selector->priv->show_details;
}

static void
directory_dnd_drag_begin_cb (GtkWidget	 *widget,
			     GdkDragContext    *context,
			     GnomeFileSelector *file_selector)
{
	gtk_drag_set_icon_pixbuf (context, file_selector->priv->folder_pixbuf, -5, -5);
}

static void
file_dnd_drag_begin_cb (GtkWidget	 *widget,
			GdkDragContext    *context,
			GnomeFileSelector *file_selector)
{
	gtk_drag_set_icon_pixbuf (context, file_selector->priv->file_pixbuf, -5, -5);
}

static void
directory_dnd_set_data_cb (GtkWidget *widget, GdkDragContext *context,
			   GtkSelectionData *selection_data, guint info,
			   guint time, GnomeFileSelector *file_selector)
{

	GtkTreeSelection *selection;
	GList *selection_list = NULL;
	GList *cur = NULL;
	GString *str = NULL;
	gchar *full_path;

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->directory_list));

	if (selection) {
		gtk_tree_selection_selected_foreach (selection,
						     selection_list_func,
						     &selection_list);
		selection_list = g_list_reverse (selection_list);
		str = g_string_new (NULL);

		for (cur = selection_list; cur; cur = cur->next) {
			full_path =
			    build_full_path (file_selector->priv->path, cur->data);
			g_string_append_printf (str, "%s\r\n", full_path);
			g_free (full_path);
		}
		gtk_selection_data_set (selection_data,
				       selection_data->target, 8, str->str,
				       strlen (str->str));
		g_list_free (selection_list);
		g_string_free (str, TRUE);

	} else
		gtk_selection_data_set (selection_data,
					selection_data->target, 8, NULL, 0);
}

static void
file_dnd_set_data_cb (GtkWidget *widget, GdkDragContext *context,
		      GtkSelectionData *selection_data, guint info,
		      guint time, GnomeFileSelector *file_selector)
{

	GtkTreeSelection *selection;
	GList *selection_list = NULL;
	GList *cur = NULL;
	GString *str = NULL;
	gchar *full_path;

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->file_list));

	if (selection) {
		gtk_tree_selection_selected_foreach (selection,
						    selection_list_func,
						    &selection_list);
		selection_list = g_list_reverse (selection_list);
		str = g_string_new (NULL);
		for (cur = selection_list; cur; cur = cur->next) {
			full_path =
			    build_full_path (file_selector->priv->path, cur->data);
			g_string_append_printf (str, "%s\r\n", full_path);
			g_free (full_path);
		}
		gtk_selection_data_set (selection_data,
				       selection_data->target, 8, str->str,
				       strlen (str->str));
		g_list_free (selection_list);
		g_string_free (str, TRUE);
	} else
		gtk_selection_data_set (selection_data,
					selection_data->target, 8, NULL, 0);
}

static GtkWidget *
init_tree_store (GnomeFileSelector *file_selector,
		 GtkWidget	 *scrolled_parent,
		 gboolean	   file_list)
{
	GtkListStore      *store;
	GtkWidget	 *tree;
	GtkCellRenderer   *renderer;
	GtkTreeViewColumn *column;

	g_return_val_if_fail (GTK_IS_SCROLLED_WINDOW (scrolled_parent), NULL);

	store = gtk_list_store_new (NUM_COLUMNS_DETAILS, GDK_TYPE_PIXBUF,
				    G_TYPE_STRING, G_TYPE_STRING,
				    G_TYPE_STRING);

	tree = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
	renderer = gtk_cell_renderer_pixbuf_new ();
	column = gtk_tree_view_column_new_with_attributes (
		"", renderer, "pixbuf", COLUMN_PIXBUF, NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
		"Name", renderer, "text", COLUMN_NAME, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

	if (file_selector->priv->show_details) {
		renderer = gtk_cell_renderer_text_new ();
		column = gtk_tree_view_column_new_with_attributes (
			"Size", renderer, "text", COLUMN_SIZE, NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);

		renderer = gtk_cell_renderer_text_new ();
		column = gtk_tree_view_column_new_with_attributes (
			"Permissions", renderer, "text", COLUMN_PERMISSIONS, NULL);
		gtk_tree_view_append_column (GTK_TREE_VIEW (tree), column);
	}

	gtk_container_add (GTK_CONTAINER (scrolled_parent), tree);
	gtk_widget_show (tree);

	return tree;
}

static void
modify_tree_columns (GnomeFileSelector *file_selector, gboolean file_list)
{
	GtkTreeView *tree;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GList *col_list;
	GList *list;
	gint col_count;

	tree = GTK_TREE_VIEW (file_list ? file_selector->priv->file_list :
			      file_selector->priv->directory_list);

	col_list = gtk_tree_view_get_columns (tree);
	col_count = g_list_length (col_list);

	if (file_selector->priv->show_details && col_count < NUM_COLUMNS_DETAILS) {
		/* we want to increase the column count */
		renderer = gtk_cell_renderer_text_new ();
		column = gtk_tree_view_column_new_with_attributes (
			"Size", renderer,
			"text", COLUMN_SIZE, NULL);
		gtk_tree_view_append_column (tree, column);

		renderer = gtk_cell_renderer_text_new ();
		column = gtk_tree_view_column_new_with_attributes (
			"Permissions", renderer,
			"text", COLUMN_PERMISSIONS, NULL);
		gtk_tree_view_append_column (tree, column);

	} else if (!file_selector->priv->show_details &&
		   col_count == NUM_COLUMNS_DETAILS) {
		/* shrink the column count */
		col_list = g_list_reverse (col_list);
		if (col_list) {
			column = (GtkTreeViewColumn *) col_list->data;
			if (column)
				gtk_tree_view_remove_column (tree, column);
			list = col_list->next;
			if (list) {
				column = (GtkTreeViewColumn *) list->data;
				if (column)
					gtk_tree_view_remove_column (tree,
								    column);
			}
		}
	}
	g_list_free (col_list);
}

static void
directory_tree_selection_changed (GtkTreeSelection  *selection,
				  GnomeFileSelector *file_selector)
{
	GList *selection_list = NULL;
	gchar *selected = NULL;

	if (file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_SINGLE ||
	    file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_MULTIPLE) {
		gtk_tree_selection_selected_foreach (selection,
						     selection_list_func,
						     &selection_list);
		if (selection_list)
			selected = selection_list->data;

		if (selected)
			gtk_entry_set_text (
				GTK_ENTRY (file_selector->priv->selection_entry),
				selected);
		else
			gtk_entry_set_text (
				GTK_ENTRY (file_selector->priv->selection_entry), "");
	}
	gtk_tree_selection_unselect_all (gtk_tree_view_get_selection
					 GTK_TREE_VIEW (file_selector->priv->file_list));
}

static void
directory_tree_row_activate (GtkTreeView       *tree_view,
			     GtkTreePath       *path,
			     GtkTreeViewColumn *column,
			     GnomeFileSelector *file_selector)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GValue value = { 0, };
	const gchar *selected;
	gchar *new_path;

	model = gtk_tree_view_get_model (tree_view);
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get_value (model, &iter, COLUMN_NAME, &value);
	selected = g_value_get_string (&value);
	new_path = build_full_path (file_selector->priv->path, selected);
	gnome_file_selector_set_directory (file_selector, new_path);
	g_free (new_path);
}

static void
file_tree_selection_changed (GtkTreeSelection *selection,
			     GnomeFileSelector *file_selector)
{
	GList *selection_list = NULL;
	gchar *selected = NULL;

	gtk_tree_selection_selected_foreach (selection, selection_list_func,
					    &selection_list);
	if (selection_list)
		selected = (gchar *) selection_list->data;
	if (selected)
		gtk_entry_set_text (GTK_ENTRY
				   (file_selector->priv->selection_entry),
				   selected);
	else
		gtk_entry_set_text (GTK_ENTRY
				   (file_selector->priv->selection_entry), "");
	gtk_tree_selection_unselect_all (gtk_tree_view_get_selection
					(GTK_TREE_VIEW
					 (file_selector->priv->directory_list)));
}

static void
file_tree_row_activate (GtkTreeView *tree_view, GtkTreePath *path,
			GtkTreeViewColumn *column,
			GnomeFileSelector *file_selector)
{
	GtkTreeModel *model;
	GtkTreeIter iter;
	GValue value = { 0, };
	gchar *selected;

	model = gtk_tree_view_get_model (tree_view);
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get_value (model, &iter, COLUMN_NAME, &value);
	selected = (gchar *) g_value_get_string (&value);
	gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry),
			    selected);
	g_signal_emit (file_selector, selector_signals[ACTION], 0, NULL);
}

static void
action_button_pressed(GtkWidget *button, GnomeFileSelector *file_selector)
{
	g_signal_emit (file_selector, selector_signals[ACTION], 0, NULL);
}

static void
cancel_button_pressed(GtkWidget *button, GnomeFileSelector *file_selector)
{
	g_signal_emit (file_selector, selector_signals[CANCEL], 0, NULL);
}

static void
hash_remove_func (gchar *key, gpointer data, gpointer d)
{
	g_object_unref (G_OBJECT (data));
}

static gint
check_complete (GtkWidget *widget, GdkEventKey *event, GnomeFileSelector *file_selector)
{
	if (event && event->keyval == GDK_Tab) {
		const gchar *text;
		text = gtk_entry_get_text (GTK_ENTRY (
			file_selector->priv->selection_entry));
		if (text && strlen(text))
			gnome_file_selector_try_complete (file_selector, text);
		return TRUE;
	}
	return FALSE;
}

static void
gnome_file_selector_try_complete (GnomeFileSelector *file_selector,
				  const gchar *complete)
{
	const gchar *p;
	gchar *path;
	gchar *append = NULL;
	gchar *ptr = NULL;
	GnomeVFSDirectoryHandle *handle = NULL;

	if (file_selector->priv->load_handle) {
		g_print ("We're already loading a directory, we "
			 "will kill the last load here.\n");
		gnome_vfs_async_cancel (file_selector->priv->load_handle);
	}

	p = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));
	path = g_strdup(p);
	if (complete[0] == '/') {
		g_free(path);
		ptr = strrchr (complete, '/');
		if (ptr != complete) {
			path = g_strndup (complete, ptr - complete);
		} else {
			path = g_strdup ("/");
		}
		ptr++;
		append = *ptr ? g_strdup (ptr) : NULL;
	} else if (complete[0] == '~') {
		g_free(path);
		path = g_strdup (g_get_home_dir ());
		if (strlen (complete) > 2 && complete[1] == '/')
			append = g_strdup (&complete[2]);
	} else if (!strcmp (complete, "..")) {
		ptr = path;
		path = build_full_path (path, complete);
		g_free (ptr);
	} else if ((ptr = strrchr (complete, '/'))) {
		gchar *temp;
		gchar *old_path;
		old_path = path;
		temp = g_strndup (complete, ptr - complete);
		path = build_full_path (path, temp);
		g_free (old_path);
		g_free (temp);
		ptr++;
		append = *ptr ? g_strdup (ptr) : NULL;
	} else {
		append = g_strdup(complete);
	}
	if (!append || (append && strlen(append) < 1)) {
		if (gnome_file_selector_set_directory (file_selector, path))
			gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry), "");
	}
	else if (gnome_vfs_directory_open(&handle, path,
					  GNOME_VFS_FILE_INFO_FOLLOW_LINKS) ==
					  GNOME_VFS_OK) {
		GnomeVFSFileInfo *finfo;
		GList *matches = NULL;
		finfo = gnome_vfs_file_info_new ();
		while (gnome_vfs_directory_read_next (handle, finfo) == GNOME_VFS_OK) {
			/*
			   Because we are nice fellows, we only dis-include '.' and '..'
			   Which essentially means that we will list dot-files even
			   if show_dots is off.
			*/
			if ((strncmp (append, finfo->name, strlen(append)) == 0) &&
			     strcmp (finfo->name, ".") && strcmp (finfo->name, "..")) {
				matches = g_list_append (matches, gnome_vfs_file_info_dup(finfo));
			}
		}
		gnome_vfs_file_info_unref (finfo);
		if (g_list_length (matches) == 1) {
			finfo = (GnomeVFSFileInfo *)matches->data;
			if (finfo->type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
				gchar *uri_path;
				uri_path = build_full_path (path, finfo->name);
				gnome_file_selector_set_directory (file_selector, uri_path);
				gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry), "");
				g_free (uri_path);
			} else if (finfo->type == GNOME_VFS_FILE_TYPE_REGULAR) {
				gtk_list_store_clear (GTK_LIST_STORE (
					gtk_tree_view_get_model (GTK_TREE_VIEW (
						file_selector->priv->directory_list))));
				gtk_list_store_clear (GTK_LIST_STORE (
					gtk_tree_view_get_model (
						GTK_TREE_VIEW (file_selector->priv->file_list))));
				insert_node (file_selector, finfo);
				gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry),
						    finfo->name);
			}
		} else if (g_list_length (matches) > 1) {
			GList *iter;
			gtk_list_store_clear (GTK_LIST_STORE (
				gtk_tree_view_get_model (GTK_TREE_VIEW (
					file_selector->priv->directory_list))));
			gtk_list_store_clear (GTK_LIST_STORE (
				gtk_tree_view_get_model (
					GTK_TREE_VIEW (file_selector->priv->file_list))));
			file_selector->priv->new_hash = g_hash_table_new (g_str_hash, g_str_equal);
			for (iter = matches; iter; iter = iter->next) {
				insert_node (file_selector, (GnomeVFSFileInfo *) iter->data);
			}
		}
		if (matches) gnome_vfs_file_info_list_free (matches);
	}
	g_free (path);
	g_free (append);
}

static gboolean
gnome_file_selector_get_listing (GnomeFileSelector *file_selector)
{
	gboolean retval = FALSE;

	if (file_selector->priv->load_handle) {
		g_print ("We're already loading a directory, we "
			 "will kill the last load here.\n");
		gnome_vfs_async_cancel (file_selector->priv->load_handle);
	}

	if (check_dir_exists (file_selector->priv->path)) {
		gtk_list_store_clear (GTK_LIST_STORE (
			gtk_tree_view_get_model (GTK_TREE_VIEW (
				file_selector->priv->directory_list))));
		gtk_list_store_clear (GTK_LIST_STORE (
			gtk_tree_view_get_model (
				GTK_TREE_VIEW (file_selector->priv->file_list))));
		file_selector->priv->new_hash = g_hash_table_new (g_str_hash, g_str_equal);

		gnome_vfs_async_load_directory (&file_selector->priv->load_handle,
					       file_selector->priv->path,
					       GNOME_VFS_FILE_INFO_FOLLOW_LINKS,
					       10, 1, directory_load_cb,
					       (gpointer) file_selector);
		retval = TRUE;
	}
	return retval;
}

static void
insert_node (GnomeFileSelector *file_selector, GnomeVFSFileInfo *finfo)
{
	GdkPixbuf *pixbuf;
	char *size_str, *perms_str;

	size_str  = gnome_vfs_format_file_size_for_display (finfo->size);
	perms_str = build_rwx_values (finfo->permissions);

	if (finfo->type == GNOME_VFS_FILE_TYPE_DIRECTORY) {
		pixbuf = file_selector->priv->folder_pixbuf;
		tree_insert_node_alphabetically (
			GTK_TREE_VIEW (file_selector->priv->directory_list),
			pixbuf, finfo->name, size_str,
			perms_str,
			file_selector->priv->show_details);
	} else {
		if (filter_match (file_selector, finfo->name)) {
			const char *icon_filename;

			icon_filename = gnome_vfs_mime_get_value (
				gnome_vfs_mime_type_from_name (finfo->name),
				"icon-filename");

			if (!icon_filename)
				pixbuf = file_selector->priv->file_pixbuf;

			else {
				pixbuf = g_hash_table_lookup (
					file_selector->priv->new_hash, icon_filename);

				if (!pixbuf)
					pixbuf = g_hash_table_lookup (
						file_selector->priv->pixmaps, icon_filename);

				if (pixbuf)
					g_object_ref (G_OBJECT (pixbuf));

				if (!pixbuf && check_file_exists (icon_filename)) {
					pixbuf = create_pixbuf_scaled (icon_filename);
				}

				if (pixbuf) {
					g_object_ref (G_OBJECT (pixbuf));
					g_hash_table_insert (file_selector->priv->new_hash,
							     (gpointer)icon_filename, pixbuf);
				} else
					pixbuf = file_selector->priv->file_pixbuf;
			}

			tree_insert_node_alphabetically (
				GTK_TREE_VIEW (file_selector->priv->file_list),
				pixbuf, finfo->name, size_str, perms_str,
				file_selector->priv->show_details);
		}
	}
	g_free (perms_str);
	g_free (size_str);
}

static GdkPixbuf *
create_pixbuf_scaled (const gchar *icon_filename)
{
	GdkPixbuf *pixbuf = NULL;

	pixbuf = gdk_pixbuf_new_from_file (icon_filename, NULL);
	if (pixbuf) {
		double pix_x, pix_y;
		gint icon_size = 20;
		pix_x = gdk_pixbuf_get_width (pixbuf);
		pix_y = gdk_pixbuf_get_height (pixbuf);
		if (pix_x > icon_size || pix_y > icon_size) {
			GdkPixbuf *scaled;
			double greatest;
			greatest = pix_x > pix_y ? pix_x : pix_y;
			scaled = gdk_pixbuf_scale_simple (pixbuf,
							 (icon_size /
							  greatest) * pix_x,
							 (icon_size /
							  greatest) * pix_y,
							  GDK_INTERP_BILINEAR);
			g_object_unref (G_OBJECT (pixbuf));
			pixbuf = scaled;
		}
	}
	return pixbuf;
}

static gchar *
get_filter (GnomeFileSelector *file_selector)
{
	const gchar *text;
	gchar *temp;
	gchar *start;
	gchar *end;
	gchar *retval;

	text = gtk_entry_get_text (GTK_ENTRY (file_selector->priv->filter_entry));
	temp = g_strdup(text);
	if ((start = strstr (temp, "(")) &&
	    (end = strstr (temp, ")")) && start < end) {
		start++;
		*end = '\0';
		retval = g_strdup(start);
		g_free(temp);
	} else {
		retval = temp;
	}
	return retval;
}

static gboolean
filter_match (GnomeFileSelector *file_selector, const gchar *filename)
{
	gchar *filter = NULL;
	gchar **sub = NULL;
	gboolean retval = FALSE;

	filter = get_filter (file_selector);
	if (filter[0] == '\0')
		retval = TRUE;
	else if ((sub = g_strsplit (filter, ",", 10))) {
		int index;
		for (index = 0; sub[index]; index++) {
			if (fnmatch (sub[index], gnome_vfs_mime_type_from_name (filename), FNM_CASEFOLD) == 0)
				retval = TRUE;
			else if (fnmatch (sub[index], filename, FNM_CASEFOLD) == 0)
				retval = TRUE;
			if (retval) break;
		}
		g_strfreev(sub);
	}
	else if (strcmp (filter, gnome_vfs_mime_type_from_name (filename)) == 0)
		retval = TRUE;
	else if (fnmatch (filter, gnome_vfs_mime_type_from_name (filename), FNM_CASEFOLD) == 0)
		retval = TRUE;
	else if (fnmatch (filter, filename, FNM_CASEFOLD) == 0)
		retval = TRUE;
	g_free (filter);
	return retval;
}

static void
directory_load_cb (GnomeVFSAsyncHandle *handle, GnomeVFSResult result,
		   GList *list, guint entries_read, gpointer callback_data)
{
	GnomeFileSelector *file_selector;
	GList *iter;
	gint count;
	GnomeVFSFileInfo *finfo;

	file_selector = GNOME_FILE_SELECTOR (callback_data);
	for (iter = list, count = 0; iter && count < entries_read;
	     iter = iter->next, count++) {
		finfo = (GnomeVFSFileInfo *) iter->data;

		/*
		 * Show all files, or all files except those with the first
		 * char == '.' unless its ".."
		 */
		if ((file_selector->priv->show_dots &&
		     strcmp (finfo->name, ".") &&
		     strcmp (finfo->name, "..")) ||
		    (!file_selector->priv->show_dots &&
		     finfo->name[0] != '.')) {
			insert_node (file_selector, finfo);
		}
	}

	if (result == GNOME_VFS_ERROR_EOF ||
	    result == GNOME_VFS_ERROR_CANCELLED) {
	    	if (result == GNOME_VFS_ERROR_CANCELLED)
	    		g_print ("GnomeVFS async load has been CANCELLED\n");
		file_selector->priv->load_handle = NULL;
		g_hash_table_foreach_remove (file_selector->priv->pixmaps,
					    (GHRFunc) hash_remove_func,
					     NULL);
		g_hash_table_destroy (file_selector->priv->pixmaps);
		file_selector->priv->pixmaps = file_selector->priv->new_hash;
		file_selector->priv->new_hash = NULL;
		gtk_widget_grab_focus (file_selector->priv->selection_entry);
		g_print ("cleanup is finished\n");
	} else if (result != GNOME_VFS_OK) {
		g_warning ("Unhandled GnomeVFS error: %s.\n",
			  gnome_vfs_result_to_string (result));
		g_hash_table_foreach_remove (file_selector->priv->new_hash,
					    (GHRFunc) hash_remove_func,
					    NULL);
		g_hash_table_destroy (file_selector->priv->new_hash);
		file_selector->priv->new_hash = NULL;
	}
}

static void
tree_insert_node_alphabetically (GtkTreeView *tree, GdkPixbuf *pixbuf,
				 gchar *name, gchar *size_str,
				 gchar *perms_str, gboolean details)
{
	GtkListStore *store;
	GtkTreeIter iter;
	GtkTreeIter sib;
	gchar *string;
	gboolean insert_set = FALSE;

	store = GTK_LIST_STORE (gtk_tree_view_get_model (tree));
	if (gtk_tree_model_get_iter_root (GTK_TREE_MODEL (store), &iter)) {
		do {
			GValue value = { 0, };

			gtk_tree_model_get_value (GTK_TREE_MODEL (store),
						 &iter, COLUMN_NAME,
						 &value);
			string = (gchar *) g_value_get_string (&value);
			if (!string || strcmp (name, string) < 0) {
				sib = iter;
				gtk_list_store_insert_before (store, &iter,
							     &sib);
				insert_set = TRUE;
				break;
			}
		} while (gtk_tree_model_iter_next
			 (GTK_TREE_MODEL (store), &iter));
	}
	if (!insert_set)
		gtk_list_store_append (store, &iter);

	gtk_list_store_set (store, &iter, COLUMN_PIXBUF, pixbuf,
			   COLUMN_NAME, name, COLUMN_SIZE, size_str,
			   COLUMN_PERMISSIONS, perms_str, -1);
}

GnomeVFSURI *
gnome_file_selector_get_uri (GnomeFileSelector * file_selector)
{
	GnomeVFSURI *uri;
	const gchar *path;
	const gchar *filename;

	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), NULL);
	g_return_val_if_fail (file_selector->priv->type != GNOME_FILE_SELECTOR_FILE_MULTIPLE &&
			      file_selector->priv->type != GNOME_FILE_SELECTOR_DIR_MULTIPLE, NULL);

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	filename = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->selection_entry));

	uri = build_uri_path (path, filename);

	return uri;
}

static void
selection_list_func (GtkTreeModel *model, GtkTreePath *path,
		     GtkTreeIter *iter, gpointer data)
{
	GList **list;
	GValue value = { 0, };
	gchar *string;

	list = (GList **) data;
	gtk_tree_model_get_value (model, iter, COLUMN_NAME, &value);
	string = (gchar *) g_value_get_string (&value);
	*list = g_list_prepend (*list, string);
}

GList *
gnome_file_selector_get_uri_list (GnomeFileSelector *file_selector)
{
	GtkTreeView *tree = NULL;
	GtkTreeSelection *selection = NULL;
	GList *selection_list = NULL;
	GList *files_list = NULL;
	GList *iter = NULL;
	const gchar *path;
	const gchar *filename;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	filename = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->selection_entry));

	if (file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_SINGLE ||
	    file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_MULTIPLE)

		tree = GTK_TREE_VIEW (file_selector->priv->directory_list);
	else
		tree = GTK_TREE_VIEW (file_selector->priv->file_list);

	selection = gtk_tree_view_get_selection (tree);

	if (selection) {
		gtk_tree_selection_selected_foreach (selection,
						     selection_list_func,
						     &selection_list);
		selection_list = g_list_reverse (selection_list);

		for (iter = selection_list; iter; iter = iter->next)
			files_list = g_list_append (
				files_list, build_uri_path (path, iter->data));

		g_list_free (selection_list);

	} else
		files_list = g_list_append (
			files_list, build_uri_path (path, filename));

	return files_list;
}

const gchar *
gnome_file_selector_get_path (GnomeFileSelector *file_selector)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), NULL);

	return file_selector->priv->path;
}

void
gnome_file_selector_set_filename (GnomeFileSelector *file_selector,
				  const gchar       *filename)
{
	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));
	gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry),
			    filename ? filename : "");
}

gboolean
gnome_file_selector_set_directory (GnomeFileSelector *file_selector,
				   const gchar       *path)
{
	gpointer temp;
	gboolean retval = FALSE;

	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), FALSE);

	temp = file_selector->priv->path;
	file_selector->priv->path = build_full_path (path, "");

	if (!file_selector->priv->path) {
		file_selector->priv->path = temp;
		return FALSE;
	}

	if (gnome_file_selector_get_listing (file_selector)) {
		gtk_entry_set_text (GTK_ENTRY (file_selector->priv->history_entry),
				    file_selector->priv->path);
		if (!temp || (temp && file_selector->priv->path &&
		    strcmp (temp, file_selector->priv->path))) {
			gnome_file_selector_history_add_string (
				file_selector, file_selector->priv->path);
		}
		retval = TRUE;
		g_free (temp);
	} else {
		g_free (file_selector->priv->path);
		file_selector->priv->path = temp;
	}
	gtk_widget_grab_focus (file_selector->priv->selection_entry);
	return retval;
}

void
gnome_file_selector_set_selector_type (GnomeFileSelector    *file_selector,
				       GnomeFileSelectorType type)
{
	GtkWidget *parent;
	GtkWidget *parents_parent;
	GnomeFileSelectorType old_type;
	GtkWidget *remove;

	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	if (file_selector->priv->type == type)
		return;

	old_type = file_selector->priv->type;
	file_selector->priv->type = type;

	if ((type == GNOME_FILE_SELECTOR_DIR_MULTIPLE ||
	     type == GNOME_FILE_SELECTOR_DIR_SINGLE) &&
	    (old_type == GNOME_FILE_SELECTOR_FILE_SINGLE ||
	     old_type == GNOME_FILE_SELECTOR_FILE_MULTIPLE)) {
		remove = file_selector->priv->directory_list->parent;
		parent = remove->parent;
		parents_parent = parent->parent;
		g_object_ref (G_OBJECT (remove));
		gtk_widget_hide (parent);
		gtk_container_remove (GTK_CONTAINER (parent), remove);
		gtk_box_pack_start (GTK_BOX (parents_parent), remove, TRUE,
				   TRUE, 0);
		g_object_unref (G_OBJECT (remove));
		gtk_widget_show (remove);

	} else if ((type == GNOME_FILE_SELECTOR_FILE_SINGLE ||
		    type == GNOME_FILE_SELECTOR_FILE_MULTIPLE) &&
		   (old_type == GNOME_FILE_SELECTOR_DIR_SINGLE ||
		    old_type == GNOME_FILE_SELECTOR_DIR_MULTIPLE)) {
		remove = file_selector->priv->directory_list->parent;
		parent = file_selector->priv->file_list->parent->parent;
		parents_parent = remove->parent;
		g_object_ref (G_OBJECT (remove));
		gtk_container_remove (GTK_CONTAINER (parents_parent),
				     remove);
		gtk_paned_add1 (GTK_PANED (parent), remove);
		g_object_unref (G_OBJECT (remove));
		gtk_widget_show_all (parent);
	}

	if (type == GNOME_FILE_SELECTOR_DIR_MULTIPLE)
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->
					      directory_list),
					     GTK_SELECTION_MULTIPLE);
	else if (type == GNOME_FILE_SELECTOR_DIR_SINGLE)
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->
					      directory_list),
					     GTK_SELECTION_SINGLE);
	else if (type == GNOME_FILE_SELECTOR_FILE_MULTIPLE) {
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->
					      directory_list),
					     GTK_SELECTION_SINGLE);
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->file_list),
					     GTK_SELECTION_MULTIPLE);
	} else if (type == GNOME_FILE_SELECTOR_FILE_SINGLE) {
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->
					      directory_list),
					     GTK_SELECTION_SINGLE);
		tree_view_set_selection_type (GTK_TREE_VIEW
					     (file_selector->priv->file_list),
					     GTK_SELECTION_SINGLE);
	}
	gtk_tree_selection_unselect_all (gtk_tree_view_get_selection
					(GTK_TREE_VIEW
					 (file_selector->priv->directory_list)));
	gtk_tree_selection_unselect_all (gtk_tree_view_get_selection
					(GTK_TREE_VIEW
					 (file_selector->priv->file_list)));
}

GnomeFileSelectorType
gnome_file_selector_get_selector_type (GnomeFileSelector *file_selector)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), 0);

	return file_selector->priv->type;
}

void
gnome_file_selector_set_action_button (GnomeFileSelector *file_selector,
				       const gchar       *stock_id)
{
	gboolean sensitive;

	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	sensitive = GTK_WIDGET_IS_SENSITIVE (file_selector->priv->action_button);
	gtk_container_remove (GTK_CONTAINER (file_selector->priv->action_area),
			      file_selector->priv->action_button);
	file_selector->priv->action_button = gtk_button_new_from_stock (stock_id);
	g_signal_connect (file_selector->priv->action_button,
			 "clicked", G_CALLBACK (action_button_pressed), file_selector);
	GTK_WIDGET_SET_FLAGS (file_selector->priv->action_button,
			      GTK_CAN_DEFAULT);
	gtk_box_pack_end (GTK_BOX (file_selector->priv->action_area),
			  file_selector->priv->action_button,
			  FALSE, FALSE, 0);
	gtk_widget_show (file_selector->priv->action_button);
	gtk_widget_set_sensitive (file_selector->priv->action_button, sensitive);
}

static void
refresh_listing (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	gnome_file_selector_set_directory (file_selector, file_selector->priv->path);
}

static void
history_go_back (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	gpointer temp = NULL;

	file_selector->priv->history_position++;
	if ( (temp =
	     g_list_nth_data (file_selector->priv->history,
			     file_selector->priv->history_position + 1))) {
		g_free (file_selector->priv->path);
		file_selector->priv->path = g_strdup (temp);
		gtk_entry_set_text (GTK_ENTRY (file_selector->priv->history_entry),
				    file_selector->priv->path);
		gnome_file_selector_get_listing (file_selector);
	}
}

static void
history_go_forward (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	if (file_selector->priv->history_position >= 0) {
		gpointer temp = NULL;

		file_selector->priv->history_position--;
		if ( (temp =
		     g_list_nth_data (file_selector->priv->history,
				     file_selector->priv->history_position +
				     1))) {
			g_free (file_selector->priv->path);
			file_selector->priv->path = g_strdup (temp);
			gtk_entry_set_text (GTK_ENTRY (file_selector->priv->history_entry),
					    file_selector->priv->path);
			gnome_file_selector_get_listing (file_selector);
		}
	}
}

static void
goto_parent (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	gchar *string;
	gchar *parent;

	string = build_full_path (file_selector->priv->path, "");
	parent = get_parent_dir (string);
	gnome_file_selector_set_directory (file_selector, parent);
	g_free (string);
	g_free (parent);
}

static void
check_action_button_cb (GtkWidget	 *widget,
			GnomeFileSelector *file_selector)
{
	const gchar *path;
	const gchar *selected;
	gchar *string;
	GtkTreeSelection *selection;
	GList *selection_list = NULL;
	gint selection_dir;
	gint selection_file;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));
	selected = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->selection_entry));

	string = build_full_path (path, selected);

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->directory_list));

	gtk_tree_selection_selected_foreach (selection, selection_list_func,
					     &selection_list);

	selection_dir = g_list_length (selection_list);
	g_list_free (selection_list);
	selection_list = NULL;
	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->file_list));

	gtk_tree_selection_selected_foreach (selection, selection_list_func,
					    &selection_list);
	selection_file = g_list_length (selection_list);
	g_list_free (selection_list);

	if (check_path_local (string) &&
	    (((check_dir_exists (string) &&
	       (file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_SINGLE ||
		file_selector->priv->type == GNOME_FILE_SELECTOR_DIR_MULTIPLE))) ||
	     (selected && strlen (selected) &&
	      (file_selector->priv->type == GNOME_FILE_SELECTOR_FILE_SINGLE ||
	       file_selector->priv->type == GNOME_FILE_SELECTOR_FILE_MULTIPLE))))
		gtk_widget_set_sensitive (file_selector->priv->action_button, TRUE);
	else
		gtk_widget_set_sensitive (file_selector->priv->action_button, FALSE);

	gtk_widget_set_sensitive (file_selector->priv->delete_button,
				  selection_dir || selection_file);

	gtk_widget_set_sensitive (file_selector->priv->rename_button, 
				  selection_dir == 1 || selection_file == 1);

	if (g_list_nth_data (file_selector->priv->history,
			     file_selector->priv->history_position + 2))
		gtk_widget_set_sensitive (file_selector->priv->back_button, TRUE);
	else
		gtk_widget_set_sensitive (file_selector->priv->back_button, FALSE);

	gtk_widget_set_sensitive (file_selector->priv->forward_button,
				  file_selector->priv->history_position >= 0);

	gtk_widget_set_sensitive (file_selector->priv->create_button,
				  check_path_local (path) && check_dir_exists (path));

	g_free (string);
}

static gchar *
get_parent_dir (const gchar *path)
{
	GnomeVFSURI *uri;
	GnomeVFSURI *new;
	gchar       *text = NULL;

	uri = gnome_vfs_uri_new (path);

	if (uri) {
		new = gnome_vfs_uri_get_parent (uri);

		if (new)
			text = gnome_vfs_uri_to_string (
				new, GNOME_VFS_URI_HIDE_NONE);
		else
			text = gnome_vfs_uri_to_string (
				uri, GNOME_VFS_URI_HIDE_NONE);

		gnome_vfs_uri_unref (uri);
	}

	return text;
}

static gchar *
build_full_path (const gchar *path, const gchar *selection)
{
	GnomeVFSURI *uri = NULL;
	GnomeVFSURI *full_uri = NULL;
	gchar *text = NULL;

	uri = gnome_vfs_uri_new (path);

	if (uri) {
		gchar *tmp;

		if (selection) {
			full_uri = gnome_vfs_uri_append_string (uri, selection);
			gnome_vfs_uri_unref (uri);
			uri = full_uri;
		}

		tmp = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
		gnome_vfs_uri_unref (uri);

		text = gnome_vfs_unescape_string (tmp, "/");
		g_free (tmp);
	}

	return text;
}

static GnomeVFSURI *
build_uri_path (const gchar *path, const gchar *selection)
{
	GnomeVFSURI *uri;
	GnomeVFSURI *full_uri;

	uri = gnome_vfs_uri_new (path);
	if (uri && selection) {
		full_uri = gnome_vfs_uri_append_string (uri, selection);
		gnome_vfs_uri_unref (uri);
		uri = full_uri;
	}

	return uri;
}

static void
check_goto_dir (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	const gchar *path;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	if (!gnome_file_selector_set_directory (file_selector, path))
		gtk_widget_grab_focus (file_selector->priv->history_entry);
}

static void
check_filter_changed (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	const gchar *text;

	text = gtk_entry_get_text (GTK_ENTRY (file_selector->priv->filter_entry));
	if (text && strchr (text, '(') && strchr (text, ')'))
		gnome_file_selector_get_listing (file_selector);
}

static void
check_filter_activate (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	gnome_file_selector_get_listing (file_selector);
}

static void
check_goto (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	gchar *string;
	const gchar *selected;
	const gchar *path;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));
	selected = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->selection_entry));

	string = build_full_path (path, selected);

	if (!check_dir_exists (string))
		g_signal_emit (file_selector, selector_signals[ACTION], 0, NULL);
	else {
		if (gnome_file_selector_set_directory (file_selector, string))
			gtk_entry_set_text (GTK_ENTRY (file_selector->priv->selection_entry), "");
	}

	g_free (string);
}

static void
create_directory (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	GtkWidget *dialog;
	GtkWidget *hbox;
	GtkWidget *image;
	GtkWidget *label;
	GtkWidget *entry;
	const gchar *path;
	gchar *new_name;
	gint response;
	GnomeVFSResult result;
	const gchar *entry_text;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	dialog = gtk_dialog_new_with_buttons (
		_("Create Directory..."), NULL,
		GTK_DIALOG_MODAL, GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL, GTK_STOCK_OK,
		GTK_RESPONSE_OK, NULL);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox,
			    FALSE, FALSE, 0);
	gtk_widget_show (hbox);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION,
					  GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 10);
	gtk_widget_show (image);

	label = gtk_label_new (_("Create:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 5);
	gtk_widget_show (label);
	entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 5);
	gtk_widget_show (entry);
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK) {
		entry_text = gtk_entry_get_text (GTK_ENTRY (entry));
		new_name = build_full_path (path, entry_text);
		result = gnome_vfs_make_directory (new_name,
						   GNOME_VFS_PERM_USER_ALL |
						   GNOME_VFS_PERM_GROUP_ALL |
						   GNOME_VFS_PERM_OTHER_READ |
						   GNOME_VFS_PERM_OTHER_EXEC);
		if (result != GNOME_VFS_OK)
			g_warning ("gnome-vfs error: %s.",
				  gnome_vfs_result_to_string (result));
		else
			gnome_file_selector_get_listing (file_selector);

		g_free (new_name);
	}
	gtk_widget_destroy (dialog);
}

static void
delete_file (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	GtkWidget *dialog;
	GtkWidget *box;
	GtkWidget *image;
	GtkWidget *label;
	const gchar *path;
	gchar *string;
	GtkTreeSelection *selection;
	GList *selection_list = NULL;
	gint selection_count;
	gint response;
	GnomeVFSResult result;
	gboolean files = FALSE;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->directory_list));

	gtk_tree_selection_selected_foreach (selection, selection_list_func,
					    &selection_list);
	if (!selection_list) {
		selection = gtk_tree_view_get_selection (
			GTK_TREE_VIEW (file_selector->priv->file_list));

		gtk_tree_selection_selected_foreach (selection,
						     selection_list_func,
						     &selection_list);
		files = TRUE;
	}
	if (!selection_list)
		return;

	selection_list = g_list_reverse (selection_list);
	selection_count = g_list_length (selection_list);

	dialog = gtk_dialog_new_with_buttons (
		_("Delete File(s)..."), NULL,
		GTK_DIALOG_MODAL, GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL, GTK_STOCK_OK,
		GTK_RESPONSE_OK, NULL);

	box = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), box, FALSE,
			   FALSE, 0);
	gtk_widget_show (box);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION,
					  GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (box), image, FALSE, FALSE, 10);
	gtk_widget_show (image);
	if (selection_count == 1)
		string = g_strdup_printf (
			_("Are you sure you want to delete '%s' ?"),
			(gchar *) selection_list->data);
	else
		string = g_strdup_printf (
			_("Are you sure you want to delete %d %s ?"),
			selection_count, files ? _("files") : _("directories"));

	label = gtk_label_new (string);
	g_free (string);

	gtk_box_pack_start (GTK_BOX (box), label, TRUE, TRUE, 5);
	gtk_widget_show (label);

	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK) {
		GList *cur;

		for (cur = selection_list; cur; cur = cur->next) {
			gchar *full_path;

			full_path = build_full_path (path, (gchar *) cur->data);
			result = gnome_vfs_unlink (full_path);
			if (result != GNOME_VFS_OK)
				g_warning ("gnome-vfs error: %s.",
					  gnome_vfs_result_to_string
					  (result));
			g_free (full_path);
		}
		gtk_entry_set_text (
			GTK_ENTRY (file_selector->priv->selection_entry), "");

		gnome_file_selector_get_listing (file_selector);
	}
	gtk_widget_destroy (dialog);
}

static void
rename_file (GtkWidget *widget, GnomeFileSelector *file_selector)
{
	GtkWidget *dialog;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *image;
	GtkWidget *label;
	GtkWidget *entry;
	const gchar *path;
	gchar *old_name;
	gchar *new_name;
	gchar *string;
	gint response;
	GnomeVFSResult result;
	const gchar *entry_text;
	GtkTreeSelection *selection;
	GList *selection_list = NULL;
	gint selection_count;
	gboolean files = FALSE;

	path = gtk_entry_get_text (
		GTK_ENTRY (file_selector->priv->history_entry));

	selection = gtk_tree_view_get_selection (
		GTK_TREE_VIEW (file_selector->priv->directory_list));

	gtk_tree_selection_selected_foreach (selection, selection_list_func,
					     &selection_list);
	if (!selection_list) {
		selection = gtk_tree_view_get_selection (
			GTK_TREE_VIEW (file_selector->priv->file_list));

		gtk_tree_selection_selected_foreach (selection,
						     selection_list_func,
						     &selection_list);
		files = TRUE;
	}
	if (!selection_list)
		return;

	selection_list = g_list_reverse (selection_list);
	selection_count = g_list_length (selection_list);

	dialog = gtk_dialog_new_with_buttons (
		_("Rename File..."), NULL,
		GTK_DIALOG_MODAL, GTK_STOCK_CANCEL,
		GTK_RESPONSE_CANCEL, GTK_STOCK_OK,
		GTK_RESPONSE_OK, NULL);

	hbox = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE,
			   FALSE, 0);
	gtk_widget_show (hbox);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION,
					  GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 10);
	gtk_widget_show (image);

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 0);
	gtk_widget_show (vbox);
	string = g_strdup_printf (_("Rename \"%s\" to:"),
				  (gchar *) selection_list->data);
	label = gtk_label_new (string);
	g_free (string);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 5);
	gtk_widget_show (label);
	entry = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (vbox), entry, TRUE, TRUE, 5);
	gtk_entry_set_text (GTK_ENTRY (entry), selection_list->data);
	gtk_widget_show (entry);
	response = gtk_dialog_run (GTK_DIALOG (dialog));
	if (response == GTK_RESPONSE_OK) {
		old_name =
		    build_full_path (path, (gchar *) selection_list->data);
		entry_text = gtk_entry_get_text (GTK_ENTRY (entry));
		new_name = build_full_path (path, entry_text);
		result = gnome_vfs_move (old_name, new_name, TRUE);
		if (result != GNOME_VFS_OK)
			g_warning ("gnome-vfs error: %s.",
				  gnome_vfs_result_to_string (result));
		gtk_entry_set_text (GTK_ENTRY
				   (file_selector->priv->selection_entry), "");
		refresh_listing (NULL, file_selector);
		g_free (old_name);
		g_free (new_name);
	}
	gtk_widget_destroy (dialog);
}

static void
toggle_show_dots (GtkToggleButton   *button,
		  GnomeFileSelector *file_selector)
{
	GdkPixbuf *pixbuf;
	GtkWidget *image;

	file_selector->priv->show_dots = button->active;
	gtk_widget_destroy (GTK_BIN (button)->child);

	pixbuf = gdk_pixbuf_new_from_xpm_data (
		button->active ? (const char **) file_selector_dots_xpm :
				 (const char **) file_selector_no_dots_xpm);
	image = gtk_image_new_from_pixbuf (pixbuf);

	gtk_container_add   (GTK_CONTAINER (button), image);
	gtk_widget_show_all (GTK_WIDGET (button));
	gnome_file_selector_get_listing (file_selector);
}

static void
toggle_show_details (GtkToggleButton   *button,
		     GnomeFileSelector *file_selector)
{
	GdkPixbuf *pixbuf;
	GtkWidget *image;

	file_selector->priv->show_details = button->active;
	gtk_widget_destroy (GTK_BIN (button)->child);

	pixbuf = gdk_pixbuf_new_from_xpm_data (
		button->active ? (const char **) file_selector_details_xpm :
				 (const char **) file_selector_no_details_xpm);
	image = gtk_image_new_from_pixbuf (pixbuf);

	gtk_container_add   (GTK_CONTAINER (button), image);
	gtk_widget_show_all (GTK_WIDGET (button));
	modify_tree_columns (file_selector, FALSE);
	modify_tree_columns (file_selector, TRUE);
}

static void
tree_view_set_selection_type (GtkTreeView *tree, GtkSelectionMode type)
{
	GtkTreeSelection *selection;

	selection = gtk_tree_view_get_selection (tree);
	gtk_tree_selection_set_mode (selection, type);
}

static gboolean
check_dir_exists (const gchar *path)
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *finfo;
	gboolean exists = FALSE;

	if (!path)
		return FALSE;

	finfo = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (path, finfo,
					  GNOME_VFS_FILE_INFO_DEFAULT);
	if (result == GNOME_VFS_OK) {
		if (finfo->type == GNOME_VFS_FILE_TYPE_DIRECTORY)
			exists = TRUE;
		gnome_vfs_file_info_unref (finfo);
	}

	return exists;
}

static gboolean
check_file_exists (const gchar *filename)
{
	GnomeVFSResult result;
	GnomeVFSFileInfo *finfo;
	gboolean exists = FALSE;

	finfo = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (filename, finfo,
					  GNOME_VFS_FILE_INFO_DEFAULT);

	if (result == GNOME_VFS_OK) {
		if (finfo->type == GNOME_VFS_FILE_TYPE_REGULAR)
			exists = TRUE;
		gnome_vfs_file_info_unref (finfo);
	}

	return exists;
}

static gboolean
check_path_local (const gchar *path)
{
	GnomeVFSURI *uri;
	gboolean local = FALSE;

	if (path) {
		uri = gnome_vfs_uri_new (path);
		if (uri) {
			if (gnome_vfs_uri_is_local (uri))
				local = TRUE;
			gnome_vfs_uri_unref (uri);
		}
	}
	return local;
}

static gchar *
build_rwx_values (GnomeVFSFilePermissions permissions)
{
	gchar *value;

	value = g_strdup ("[---][---][---]");

	if (permissions & GNOME_VFS_PERM_USER_READ)
		value[1] = 'r';
	if (permissions & GNOME_VFS_PERM_USER_WRITE)
		value[2] = 'w';
	if (permissions & GNOME_VFS_PERM_USER_EXEC)
		value[3] = 'x';
	if (permissions & GNOME_VFS_PERM_GROUP_READ)
		value[6] = 'r';
	if (permissions & GNOME_VFS_PERM_GROUP_WRITE)
		value[7] = 'w';
	if (permissions & GNOME_VFS_PERM_GROUP_EXEC)
		value[8] = 'x';
	if (permissions & GNOME_VFS_PERM_OTHER_READ)
		value[11] = 'r';
	if (permissions & GNOME_VFS_PERM_OTHER_WRITE)
		value[12] = 'w';
	if (permissions & GNOME_VFS_PERM_OTHER_EXEC)
		value[13] = 'x';

	return value;
}

static void
register_selector_for_notification (GnomeFileSelector *file_selector)
{
	GConfClient *client;
	GError *error = NULL;

	client = gconf_extensions_client_get ();
	g_return_if_fail (client != NULL);

	gconf_client_add_dir (client, "/apps/nautilus/preferences",
			      GCONF_CLIENT_PRELOAD_NONE, NULL);
	file_selector->priv->gconf_connection = gconf_client_notify_add (
		client, "/apps/nautilus/preferences/theme",
		(GConfClientNotifyFunc) nautilus_theme_changed, file_selector,
		(GFreeFunc) NULL, &error);
}

static void
nautilus_theme_changed (GConfClient       *client,
			guint	      cnxn_id,
			GConfEntry	*entry,
			GnomeFileSelector *file_selector)
{
	const gchar *key;
	GConfValue  *value;
	const gchar *keyvalue;

	g_return_if_fail (client != NULL);
	g_return_if_fail (entry != NULL);

	key = gconf_entry_get_key (entry);
	value = gconf_entry_get_value (entry);
	keyvalue = gconf_value_get_string (value);
	g_print (" (nautilus-theme-changed) key - value pair is %s - %s\n",
		 key, keyvalue);
	file_selector_setup_default_icons (file_selector);
	gnome_file_selector_get_listing (file_selector);
}

static void
file_selector_setup_default_icons (GnomeFileSelector *file_selector)
{
	gchar *keyvalue;

	g_return_if_fail (file_selector != NULL);

	if (file_selector->priv->folder_pixbuf) {
		g_object_unref (G_OBJECT (file_selector->priv->folder_pixbuf));
		file_selector->priv->folder_pixbuf = NULL;
	}

	if (file_selector->priv->file_pixbuf) {
		g_object_unref (G_OBJECT (file_selector->priv->file_pixbuf));
		file_selector->priv->file_pixbuf = NULL;
	}

	keyvalue = gconf_extensions_get_string ("/apps/nautilus/preferences", "theme");

	if (keyvalue && strlen (keyvalue) > 1) {
		g_print ("Current nautilus theme is %s\n", keyvalue);
		set_icons_from_nautilus_theme (file_selector, keyvalue);
	}

	if (!file_selector->priv->folder_pixbuf)
		file_selector->priv->folder_pixbuf =
		    gdk_pixbuf_new_from_xpm_data (
			    (const gchar **) file_selector_folder_xpm);

	if (!file_selector->priv->file_pixbuf)
		file_selector->priv->file_pixbuf =
		    gdk_pixbuf_new_from_xpm_data (
			    (const gchar **) file_selector_file_xpm);
}

static void
set_icons_from_nautilus_theme (GnomeFileSelector *file_selector,
			       const gchar       *theme)
{
	gchar *path;
	gchar *file;
	GdkPixbuf *unscaled;

	if (!strcmp (theme, "default"))
		path = g_strdup (DATADIR "/pixmaps/nautilus/");
	else
		path = g_strdup_printf (DATADIR "/pixmaps/nautilus/%s/", theme);

	if (check_dir_exists (path)) {
		file = g_strconcat (path, "i-directory-20-aa.png", NULL);
		if (check_file_exists (file)) {
			file_selector->priv->folder_pixbuf =
			    gdk_pixbuf_new_from_file (file, NULL);
		} else {
			g_free (file);
			file = g_strconcat (path, "i-directory-aa.png", NULL);
			if (check_file_exists (file)) {
				file_selector->priv->folder_pixbuf = create_pixbuf_scaled (file);
			}
		}
		g_free (file);

		file = g_strconcat (path, "i-regular-20-aa.png", NULL);
		if (check_file_exists (file))
			file_selector->priv->file_pixbuf = create_pixbuf_scaled (file);
		else {
			g_free (file);
			file = g_strconcat (path, "i-regular-aa.png", NULL);
			if (check_file_exists (file)) {
				file_selector->priv->file_pixbuf = create_pixbuf_scaled (file);
			}
		}
		g_free (file);
	}
	g_free (path);
}

void
gnome_file_selector_set_history_id (GnomeFileSelector *file_selector,
				    const gchar       *history_id)
{
	GList *iter;

	if (file_selector->priv->history_id)
		g_free (file_selector->priv->history_id);

	for (iter = file_selector->priv->history; iter; iter = iter->next)
		g_free (iter->data);

	g_list_free (file_selector->priv->history);

	if (history_id) {
		file_selector->priv->history_id = g_strdup (history_id);
		inplace_replace (file_selector->priv->history_id, ' ', '-');
	}
	gnome_file_selector_load_history (file_selector);
}

void
gnome_file_selector_set_mime_types (GnomeFileSelector    *file_selector,
				    const gchar	  *mime_types)
{
	GList *mime_list = NULL;
	gchar **mimes;
	gint index;
	gchar **full_type;
	gchar *description;

	if(file_selector->priv->mime_types)
	{
		GList *cur = NULL;
		for(cur = file_selector->priv->mime_types; cur; cur = cur->next) {
			g_free(cur->data);
		}
		g_list_free(file_selector->priv->mime_types);
	}
	mimes = g_strsplit(mime_types, "|", 25);
	for (index = 0; mimes[index]; index++) {
		full_type = g_strsplit(mimes[index], ":", 2);
		if (full_type) {
			description = g_strdup_printf ("%s (%s)", full_type[0], full_type[1]);
			g_print ("(set-mime-types) description is %s\n", description);
			mime_list = g_list_append (mime_list, description);
			g_strfreev (full_type);
		}
	}
	g_strfreev(mimes);
	file_selector->priv->mime_types = mime_list;
	gtk_combo_set_popdown_strings (GTK_COMBO (file_selector->priv->filter_combo), mime_list);
}

static void
inplace_replace (gchar *string, gchar replace, gchar with)
{
	gint index;

	if (!string || strlen (string) < 1)
		return;

	for (index = 0; string[index]; index++) {
		if (string[index] == replace)
			string[index] = with;
	}
}
