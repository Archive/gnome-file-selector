/*  gnome-file-selector-factory.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <ctype.h>
#include <string.h>
#include <libbonobo.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo-activation/bonobo-activation.h>
#include <bonobo/bonobo-item-handler.h>
#include <bonobo/bonobo-property-bag.h>
#include <libbonoboui.h>

#include "gnome-file-selector.h"

enum {
	PROP_APPLICATION,
	PROP_ENABLE_VFS,
	PROP_MULTIPLE_SELECTION,
	PROP_SAVE_MODE,
	PROP_MIME_TYPES,
	PROP_DEFAULT_LOCATION,
	PROP_DEFAULT_FILENAME,
};

static BonoboObject  *gnome_file_selector_factory      (BonoboGenericFactory *factory,
							const char           *component_id,
							gpointer              closure);
static Bonobo_Unknown gnome_file_selector_item_handler (BonoboItemHandler    *handler,
							const char           *item_name,
							gboolean              only_if_exists,
							gpointer              data,
							CORBA_Environment     *ev);

static void gnome_file_selector_set_prop(BonoboPropertyBag *bag, const BonoboArg *arg, guint arg_id, CORBA_Environment *ev, BonoboControl *control);
static void gnome_file_selector_get_prop(BonoboPropertyBag *bag, BonoboArg *arg, guint arg_id, CORBA_Environment *ev, BonoboControl *control);
static void action_button_cb(GtkWidget *button, BonoboControl *control);
static void cancel_button_cb(GtkWidget *button, BonoboControl *control);

BONOBO_ACTIVATION_SHLIB_FACTORY ("OAFIID:GNOME_FileSelector_Control_Factory",
                                 "GnomeFileSelector Factory", gnome_file_selector_factory, NULL);

static BonoboObject *
gnome_file_selector_factory (BonoboGenericFactory *factory,
			     const char           *component_id,
			     gpointer              closure)
{
	GtkWidget         *file_selector;
	BonoboControl     *control;
	BonoboEventSource *event_source;
	BonoboPropertyBag *properties;
	BonoboItemHandler *handler;

	file_selector = gnome_file_selector_new (
		NULL, GNOME_FILE_SELECTOR_FILE_SINGLE, GTK_STOCK_OPEN);

	gtk_widget_show (file_selector);
	control = bonobo_control_new (file_selector);

	event_source = bonobo_event_source_new ();
	bonobo_object_add_interface (BONOBO_OBJECT (control), 
				     BONOBO_OBJECT (event_source));

	properties = bonobo_property_bag_new (
		(BonoboPropertyGetFn) gnome_file_selector_get_prop,
		(BonoboPropertySetFn) gnome_file_selector_set_prop,
		control);

	bonobo_property_bag_add (properties, "Application", PROP_APPLICATION,
				 BONOBO_ARG_STRING, NULL,
				 _("Who is our parent?"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "EnableVFS", PROP_ENABLE_VFS,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Enable the Virtual File System?"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "MultipleSelection", PROP_MULTIPLE_SELECTION,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Single or Multiple selection?"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "SaveMode", PROP_SAVE_MODE,
				 BONOBO_ARG_BOOLEAN, NULL,
				 _("Save or Open?"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "MimeTypes", PROP_MIME_TYPES,
				 BONOBO_ARG_STRING, NULL,
				 _("Mime-types to list in the selection"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "DefaultLocation", PROP_DEFAULT_LOCATION,
				 BONOBO_ARG_STRING, NULL,
				 _("Location to start from"),
				 BONOBO_PROPERTY_WRITEABLE);
	bonobo_property_bag_add (properties, "DefaultFileName", PROP_DEFAULT_FILENAME,
				 BONOBO_ARG_STRING, NULL,
				 _("Filename to set on initialization"),
				 BONOBO_PROPERTY_WRITEABLE);

	bonobo_control_set_properties (control, BONOBO_OBJREF (properties), NULL);
	bonobo_object_unref (BONOBO_OBJECT (properties));

	handler = bonobo_item_handler_new (NULL, gnome_file_selector_item_handler, properties);
	bonobo_object_add_interface (BONOBO_OBJECT (control), BONOBO_OBJECT (handler));
	g_signal_connect (G_OBJECT (GNOME_FILE_SELECTOR (file_selector)),
			 "action", G_CALLBACK (action_button_cb), control);
	g_signal_connect (G_OBJECT (GNOME_FILE_SELECTOR (file_selector)),
			 "cancel", G_CALLBACK (cancel_button_cb), control);
	return BONOBO_OBJECT (control);
}

static Bonobo_Unknown
gnome_file_selector_item_handler (BonoboItemHandler    *handler,
				  const char           *item_name,
				  gboolean              only_if_exists,
				  gpointer              data,
				  CORBA_Environment     *ev)
{
	BonoboPropertyBag *pb;
	GSList *options;
	GSList *l;
	BonoboItemOption *o;
	BonoboArg *arg;

	pb = data;

	options = bonobo_item_option_parse (item_name);

	for (l = options; l; l = l->next) {
		BonoboArgType arg_type;
		o = l->data;

		arg_type = Bonobo_PropertyBag_getType (BONOBO_OBJREF (pb), o->key, ev);
		if (!arg_type)
			continue;
		arg = bonobo_arg_new (arg_type);
		if (!arg)
			continue;

		switch (arg_type->kind) {
		case CORBA_tk_boolean:
			BONOBO_ARG_SET_BOOLEAN (arg, tolower ((int)o->value[0]) == 't' ||
						tolower ((int)o->value[0]) == 'y' ||
						atoi (o->value) ? CORBA_TRUE : CORBA_FALSE);
			break;
		case CORBA_tk_long:
			BONOBO_ARG_SET_LONG (arg, atol (o->value));
			break;
		case CORBA_tk_float:
			BONOBO_ARG_SET_FLOAT (arg, atof (o->value));
			break;
		case CORBA_tk_double:
			BONOBO_ARG_SET_DOUBLE (arg, atof (o->value));
			break;
		case CORBA_tk_string:
			BONOBO_ARG_SET_STRING (arg, o->value);
			break;
		default:
			bonobo_arg_release (arg);
			continue;
		}
		Bonobo_PropertyBag_setValue (BONOBO_OBJREF (pb), o->key, arg, ev);
		bonobo_arg_release (arg);
	}

	return bonobo_object_dup_ref (BONOBO_OBJREF (handler), ev);
}

static void
gnome_file_selector_set_prop (BonoboPropertyBag *bag,
			      const BonoboArg   *arg,
			      guint              arg_id,
			      CORBA_Environment *ev,
			      BonoboControl     *control)
{
	GnomeFileSelector *file_selector;
	gchar *value;
	gboolean bool_value;
	GnomeFileSelectorType type;

	file_selector = (GnomeFileSelector *) bonobo_control_get_widget (control);

	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	switch (arg_id) {
        case PROP_APPLICATION:
		value = BONOBO_ARG_GET_STRING (arg);
		gnome_file_selector_set_history_id (file_selector, value);
		break;
        case PROP_ENABLE_VFS:
		bool_value = BONOBO_ARG_GET_BOOLEAN (arg);
		break;
        case PROP_MULTIPLE_SELECTION:
		bool_value = BONOBO_ARG_GET_BOOLEAN (arg);
		type = gnome_file_selector_get_selector_type (file_selector);
		if ((type == GNOME_FILE_SELECTOR_FILE_SINGLE && bool_value))
			gnome_file_selector_set_selector_type (file_selector, GNOME_FILE_SELECTOR_FILE_MULTIPLE);
		else if ((type == GNOME_FILE_SELECTOR_FILE_MULTIPLE && !bool_value))
			gnome_file_selector_set_selector_type (file_selector, GNOME_FILE_SELECTOR_FILE_SINGLE);
		break;
        case PROP_SAVE_MODE:
		bool_value = BONOBO_ARG_GET_BOOLEAN (arg);
		gnome_file_selector_set_action_button (file_selector, bool_value ? GTK_STOCK_SAVE : GTK_STOCK_OPEN);
		break;
        case PROP_MIME_TYPES:
		value = BONOBO_ARG_GET_STRING (arg);
		gnome_file_selector_set_mime_types (file_selector, value);
		break;
        case PROP_DEFAULT_LOCATION:
		value = BONOBO_ARG_GET_STRING (arg);
		gnome_file_selector_set_directory (file_selector, (gchar *) (value ? value : g_get_home_dir ()));
		break;
        case PROP_DEFAULT_FILENAME:
		value = BONOBO_ARG_GET_STRING (arg);
		gnome_file_selector_set_filename(file_selector, (const gchar *) value);
		break;
        default:
		break;
	}
}

static void 
gnome_file_selector_get_prop (BonoboPropertyBag *bag,
			      BonoboArg         *arg,
			      guint              arg_id,
			      CORBA_Environment *ev,
			      BonoboControl     *control)
{
    switch (arg_id) {
        default:
		break;
    }
}

static BonoboEventSource *
get_event_source (BonoboControl *control)
{
	return BONOBO_EVENT_SOURCE (
		bonobo_object_query_local_interface (
			BONOBO_OBJECT (control),
			"IDL:Bonobo/EventSource:1.0"));
}

static void
action_button_cb (GtkWidget *button, BonoboControl *control)
{
	GnomeFileSelector *file_selector;
	BonoboEventSource *event_source;

	g_print ("Action button pressed: %p\n", button);
	file_selector = GNOME_FILE_SELECTOR (bonobo_control_get_widget (control));

	event_source = get_event_source (control);

	if (event_source) {
		GList *uri_list;

		uri_list = gnome_file_selector_get_uri_list (file_selector);

		if (g_list_length (uri_list)) {
			gint   i;
			GList *iter;
			CORBA_any any;
			CORBA_sequence_CORBA_string seq;

			seq._release = FALSE;
			seq._length = g_list_length (uri_list);
			seq._buffer = g_newa (CORBA_char *, seq._length);

			for (iter = uri_list, i = 0; iter; iter = iter->next, i++) {
				GnomeVFSURI *uri = iter->data;
				gchar *text_uri;

				text_uri = gnome_vfs_uri_to_string (uri, GNOME_VFS_URI_HIDE_NONE);
				gnome_vfs_uri_unref (uri);

				seq._buffer[i] = (CORBA_string) text_uri;
			}
			g_list_free (uri_list);

			any._type = TC_CORBA_sequence_CORBA_string;
			any._value = &seq;
			any._release = FALSE;

			bonobo_event_source_notify_listeners_full (
				event_source, "GNOME/FileSelector/Control", "ButtonClicked",
				"Action", &any, NULL);

			for (i = 0; i < seq._length; i++)
				if (seq._buffer[i])
					g_free (seq._buffer[i]);

		}
		bonobo_object_unref (BONOBO_OBJECT (event_source));
	}
}

static void
cancel_button_cb (GtkWidget *button, BonoboControl *control)
{
	GnomeFileSelector *file_selector;
	BonoboEventSource *event_source;

	g_print ("Cancel button pressed\n");

	file_selector = GNOME_FILE_SELECTOR (bonobo_control_get_widget (control));
	event_source = get_event_source (control);
	
	if (event_source) {
		bonobo_event_source_notify_listeners_full (
			event_source, "GNOME/FileSelector/Control", "ButtonClicked",
			"Cancel", NULL, NULL);
		bonobo_object_unref (BONOBO_OBJECT (event_source));
	}
}
