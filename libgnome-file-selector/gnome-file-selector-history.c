/*  gnome-file-selection-history.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <glib.h>
#include <string.h>

#include "gnome-file-selector.h"
#include "gnome-file-selector-gconf.h"
#include "gnome-file-selector-private.h"
#include "gnome-file-selector-history.h"

static void check_history_list_length (GnomeFileSelector *file_selector);

void
gnome_file_selector_set_max_history (GnomeFileSelector *file_selector, gint history_max)
{
	g_return_if_fail (history_max > 0);
	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));
    
	file_selector->priv->history_max = history_max;
	check_history_list_length (file_selector);
}

gint
gnome_file_selector_get_max_history (GnomeFileSelector *file_selector)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), -1);

	return file_selector->priv->history_max;
}

gboolean
gnome_file_selector_history_add_string (GnomeFileSelector *file_selector,
					const gchar       *string)
{
	g_return_val_if_fail (GNOME_IS_FILE_SELECTOR (file_selector), FALSE);

	file_selector->priv->history = g_list_prepend (file_selector->priv->history, g_strdup (string));
	check_history_list_length (file_selector);
	gtk_combo_set_popdown_strings (GTK_COMBO (file_selector->priv->history_combo), file_selector->priv->history);
	file_selector->priv->history_position = -1;

	return TRUE;
}

void
gnome_file_selector_history_add_strings (GnomeFileSelector *file_selector, GList *list)
{
	g_return_if_fail (GNOME_IS_FILE_SELECTOR (file_selector));

	file_selector->priv->history = g_list_concat (list, file_selector->priv->history);
	check_history_list_length (file_selector);

	gtk_combo_set_popdown_strings (GTK_COMBO (file_selector->priv->history_combo),
				       file_selector->priv->history);
	file_selector->priv->history_position = -1;
}

static void
check_history_list_length (GnomeFileSelector *file_selector)
{
	int i;
	GList *l, *next;

	i = 0;
	for (l = file_selector->priv->history; l && i < file_selector->priv->history_max; l = l->next)
		i++;

	for (; l; l = next) {
		next = l->next;
		g_free (l->data);
		file_selector->priv->history = g_list_delete_link (file_selector->priv->history, l);
	}
}

void
gnome_file_selector_load_history (GnomeFileSelector *file_selector)
{
	gchar *prefix = NULL;
	gchar *text = NULL;
	GList *list = NULL;

	if (!file_selector->priv->history_id)
		return;

	prefix = g_strdup_printf ("/apps/gnome-file-selector/%s",
				   file_selector->priv->history_id);

	if (gconf_client_dir_exists (gconf_extensions_client_get (), prefix, NULL)) {
		GList *list;
		file_selector->priv->show_dots = gconf_extensions_get_bool (
			prefix, "show-dots");
		file_selector->priv->show_details = gconf_extensions_get_bool (
			prefix, "show-details");
		file_selector->priv->history_max = gconf_extensions_get_int (
			prefix, "max-history");
		file_selector->priv->history = gconf_extensions_get_string_list (
			prefix, "history");
	}
	g_free (prefix);
}

void
gnome_file_selector_save_history (GnomeFileSelector *file_selector)
{
	gchar *prefix;
	GList *iter;

	if (!file_selector->priv->history_id)
		return;

	prefix = g_strdup_printf ("/apps/gnome-file-selector/%s",
				   file_selector->priv->history_id);

	gconf_extensions_set_bool (prefix, "show-dots", file_selector->priv->show_dots);
	gconf_extensions_set_bool (prefix, "show-details", file_selector->priv->show_details);
	gconf_extensions_set_int (prefix, "max-history", file_selector->priv->history_max);

	if (g_list_length (file_selector->priv->history)) {
		gconf_extensions_set_string_list (prefix, "history", file_selector->priv->history);
	}
	g_free (prefix);
}
