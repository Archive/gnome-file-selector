/*  gnome-file-selector-gconf.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GNOME_FILE_SELECTOR_GCONF_H_
#define _GNOME_FILE_SELECTOR_GCONF_H_

#include <glib.h>
#include <gconf/gconf.h>
#include <gconf/gconf-client.h>

G_BEGIN_DECLS

void		 gconf_extensions_client_setup (void);
void		 gconf_extensions_client_free  (void);
GConfClient	*gconf_extensions_client_get   (void);

void		 gconf_extensions_set_bool     (const char *prefix,
						const char *key,
						gboolean bool_value);
gboolean	 gconf_extensions_get_bool     (const char *prefix,
						const char *key);
void		 gconf_extensions_set_int      (const char *prefix,
						const char *key,
						gint int_value);
gint		 gconf_extensions_get_int      (const char *prefix,
						const char *key);

void		 gconf_extensions_set_string   (const char *prefix,
						const char *key,
						const char *string_value);
char 		*gconf_extensions_get_string   (const char *prefix,
						const char *key);

void		 gconf_extensions_set_string_list (const char *prefix,
						   const char *key,
						   GList *strings);
GList		*gconf_extensions_get_string_list (const char *prefix,
						   const char *key);

gboolean	 gconf_extensions_handle_error (GError **error);

G_END_DECLS

#endif
