/*  gnome-file-selector-private.h
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GNOME_FILE_SELECTOR_PRIVATE_H_
#define _GNOME_FILE_SELECTOR_PRIVATE_H_

#include <glib.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>

G_BEGIN_DECLS

struct _GnomeFileSelectorPrivate {
	GnomeFileSelectorType type;
	gchar *history_id;
	gboolean show_dots;
	gboolean show_details;
	gint history_max;
	
	GtkWidget *back_button;
	GtkWidget *forward_button;
	GtkWidget *create_button;
	GtkWidget *delete_button;
	GtkWidget *rename_button;
	GtkWidget *dots_toggle;
	GtkWidget *details_toggle;
	GtkWidget *history_combo;
	GtkWidget *history_entry;
	GtkWidget *directory_list;
	GtkWidget *file_list;
	GtkWidget *selection_entry;
	GtkWidget *filter_combo;
	GtkWidget *filter_entry;
	GtkWidget *action_area;
	GtkWidget *action_button;
	GtkWidget *cancel_button;
	
	GdkPixbuf *folder_pixbuf;
	GdkPixbuf *file_pixbuf;
	
	gchar *path;
	GList *history;
	gint history_position;
	GList *mime_types;

	guint gconf_connection;
	GHashTable *pixmaps;
	
	/* if set, we're loading */
	GnomeVFSAsyncHandle *load_handle;
	GHashTable *new_hash;
};

G_END_DECLS

#endif
