/*  gnome-file-selector.h
 *
 *  Gnome-Enabled VFS Based File Selector based on GTK+ 2.0
 *  Copyright (C) 2001 Chris Phelps <chicane@reninet.com
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-types.h>
#include <libgnomevfs/gnome-vfs-uri.h>

#ifndef _GNOME_FILE_SELECTOR_H_
#define _GNOME_FILE_SELECTOR_H_

G_BEGIN_DECLS

#define GNOME_TYPE_FILE_SELECTOR (gnome_file_selector_get_type())
#define GNOME_FILE_SELECTOR(obj) (GTK_CHECK_CAST((obj), GNOME_TYPE_FILE_SELECTOR, GnomeFileSelector))
#define GNOME_FILE_SELECTOR_CLASS(klass) (GTK_CHECK_CLASS_CAST((klass), GNOME_TYPE_FILE_SELECTOR, GnomeFileSelectorClass))
#define GNOME_IS_FILE_SELECTOR(obj) (GTK_CHECK_TYPE((obj), GNOME_TYPE_FILE_SELECTOR))
#define GNOME_IS_FILE_SELECTOR_CLASS(klass) (GTK_CHECK_CLASS_TYPE((klass), GNOME_TYPE_FILE_SELECTOR))

typedef enum {
	GNOME_FILE_SELECTOR_FILE_SINGLE,
	GNOME_FILE_SELECTOR_FILE_MULTIPLE,
	GNOME_FILE_SELECTOR_DIR_SINGLE,
	GNOME_FILE_SELECTOR_DIR_MULTIPLE
} GnomeFileSelectorType;

typedef struct _GnomeFileSelector        GnomeFileSelector;
typedef struct _GnomeFileSelectorPrivate GnomeFileSelectorPrivate;
typedef struct _GnomeFileSelectorClass   GnomeFileSelectorClass;

struct _GnomeFileSelector {
	GtkVBox top;
	GnomeFileSelectorPrivate *priv;
};

struct _GnomeFileSelectorClass {
	GtkVBoxClass parent_class;

	void (*action) (GnomeFileSelector *file_selector);
	void (*cancel) (GnomeFileSelector *file_selector);
};

GType                 gnome_file_selector_get_type          (void);
GtkWidget            *gnome_file_selector_construct         (GnomeFileSelector    *file_selector,
							     const gchar          *history_id,
							     const gchar          *path,
							     GnomeFileSelectorType type,
							     const gchar          *stock_id);
/* make a new widget */
GtkWidget            *gnome_file_selector_new               (const gchar          *history_id,
							     GnomeFileSelectorType type,
							     const gchar          *stock_id);
/* start it off with a particular path */
GtkWidget            *gnome_file_selector_new_with_path     (const gchar          *history_id,
							     const gchar          *path,
							     GnomeFileSelectorType type,
							     const gchar          *stock_id);
GnomeVFSURI          *gnome_file_selector_get_uri           (GnomeFileSelector    *file_selector);
GList                *gnome_file_selector_get_uri_list      (GnomeFileSelector    *file_selector);
const gchar          *gnome_file_selector_get_path          (GnomeFileSelector    *file_selector);

void		      gnome_file_selector_set_filename	    (GnomeFileSelector	  *file_selector,
							     const gchar *filename);
gboolean              gnome_file_selector_set_directory     (GnomeFileSelector    *file_selector,
							     const gchar          *path);

void                  gnome_file_selector_set_selector_type (GnomeFileSelector    *file_selector,
							     GnomeFileSelectorType type);
GnomeFileSelectorType gnome_file_selector_get_selector_type (GnomeFileSelector    *file_selector);
void                  gnome_file_selector_set_action_button (GnomeFileSelector    *file_selector,
							     const gchar          *stock_id);

void                  gnome_file_selector_set_history_id    (GnomeFileSelector    *file_selector,
							     const gchar          *history_id);

void                  gnome_file_selector_set_mime_types    (GnomeFileSelector    *file_selector,
							     const gchar          *mime_types);

G_END_DECLS

#endif
