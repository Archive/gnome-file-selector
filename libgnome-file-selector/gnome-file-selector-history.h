/*  gnome-file-selection-history.h
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>

#ifndef _GNOME_FILE_SELECTOR_HISTORY_H_
#define _GNOME_FILE_SELECTOR_HISTORY_H_

G_BEGIN_DECLS

gint      gnome_file_selector_get_max_history     (GnomeFileSelector *file_selector);
void      gnome_file_selector_set_max_history     (GnomeFileSelector *file_selector,
						   gint               history_max);
gboolean  gnome_file_selector_history_add_string  (GnomeFileSelector *file_selector,
						   const gchar       *string);
void      gnome_file_selector_history_add_strings (GnomeFileSelector *file_selector,
						   GList             *list);

void      gnome_file_selector_load_history        (GnomeFileSelector *file_selector);
void      gnome_file_selector_save_history        (GnomeFileSelector *file_selector);

G_END_DECLS

#endif

