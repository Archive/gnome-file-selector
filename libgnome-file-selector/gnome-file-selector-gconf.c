/*  gnome-file-selector-gconf.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib.h>
#include <gconf/gconf-client.h>
#include <gconf/gconf-value.h>

#include "gnome-file-selector-gconf.h"

static GConfClient  *gconf_client = NULL;

void
gconf_extensions_client_setup (void)
{
	if (!gconf_is_initialized ()) {
		char *argv[] = { "gnome-file-selector", NULL };

		if (!gconf_init (1, argv, NULL))
			g_error ("GConf init failed");
	}

	if (gconf_client == NULL) {
		gconf_client = gconf_client_get_default ();
		g_atexit (gconf_extensions_client_free);
	}
}

void
gconf_extensions_client_free (void)
{
	if (gconf_client != NULL) {
		g_object_unref (G_OBJECT (gconf_client));
		gconf_client = NULL;
	}
}

GConfClient *
gconf_extensions_client_get (void)
{
	return gconf_client;
}

void
gconf_extensions_set_bool (const char *prefix, const char *key, gboolean bool_value)
{
	GConfClient *client;
	char *full;
	GError *error = NULL;

	g_return_if_fail (prefix != NULL);
	g_return_if_fail (key != NULL);

	client = gconf_extensions_client_get ();
	g_return_if_fail (client != NULL);

	full = g_strconcat (prefix, "/", key, NULL);
	gconf_client_set_bool (client, full, bool_value, &error);
	g_free (full);
	gconf_extensions_handle_error (&error);
}

gboolean
gconf_extensions_get_bool (const char *prefix, const char *key)
{
	GConfClient *client;
	char *full;
	gboolean result;
	GError *error = NULL;

	g_return_val_if_fail (prefix != NULL, FALSE);
	g_return_val_if_fail (key != NULL, FALSE);

	client = gconf_extensions_client_get ();
	g_return_val_if_fail (client != NULL, FALSE);

	full = g_strconcat (prefix, "/", key, NULL);
	result = gconf_client_get_bool (client, full, &error);
	g_free (full);
	gconf_extensions_handle_error (&error);
	return result;
}

void
gconf_extensions_set_int (const char *prefix, const char *key, gint int_value)
{
	GConfClient *client;
	char *full;
	GError *error = NULL;

	g_return_if_fail (prefix != NULL);
	g_return_if_fail (key != NULL);

	client = gconf_extensions_client_get ();
	g_return_if_fail (client != NULL);

	full = g_strconcat (prefix, "/", key, NULL);
	gconf_client_set_int (client, full, int_value, &error);
	g_free (full);
	gconf_extensions_handle_error (&error);
}

gint
gconf_extensions_get_int (const char *prefix, const char *key)
{
	GConfClient *client;
	char *full;
	gint result;
	GError *error = NULL;

	g_return_val_if_fail (prefix != NULL, 0);
	g_return_val_if_fail (key != NULL, 0);

	client = gconf_extensions_client_get ();
	g_return_val_if_fail (client != NULL, 0);

	full = g_strconcat (prefix, "/", key, NULL);
	result = gconf_client_get_int (client, full, &error);
	g_free (full);
	gconf_extensions_handle_error (&error);
	return result;
}

void
gconf_extensions_set_string (const char *prefix, const char *key, const char *string_value)
{
	GConfClient *client;
	char *full;
	GError *error = NULL;

	g_return_if_fail (prefix != NULL);
	g_return_if_fail (key != NULL);
	g_return_if_fail (string_value != NULL);

	client = gconf_extensions_client_get ();
	g_return_if_fail (client != NULL);

	full = g_strconcat (prefix, "/", key, NULL);
	gconf_client_set_string (client, full, string_value, &error);
	g_free (full);
	gconf_extensions_handle_error (&error);
}

char *
gconf_extensions_get_string (const char *prefix, const char *key)
{
	GConfClient *client;
	char *full;
	char *result;
	GError *error = NULL;

	g_return_val_if_fail (prefix != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);

	client = gconf_extensions_client_get ();
	g_return_val_if_fail (client != NULL, NULL);

	full = g_strconcat (prefix, "/", key, NULL);
	result = gconf_client_get_string (client, full, &error);
	g_free (full);
	if (gconf_extensions_handle_error (&error))
		result = g_strdup ("");
	return result;
}

void
gconf_extensions_set_string_list (const char *prefix,
				  const char *key,
				  GList *strings)
{
	GConfClient *client;
	char *full = NULL;
	GSList *list = NULL;
	GList *iter = NULL;
	GError *error = NULL;

	g_return_if_fail (prefix != NULL);
	g_return_if_fail (key != NULL);

	client = gconf_extensions_client_get ();
	g_return_if_fail (client != NULL);

	for (iter = strings; iter; iter = iter->next) {
		list = g_slist_append (list, iter->data);
	}
	full = g_strconcat (prefix, "/", key, NULL);
	gconf_client_set_list (client, full, GCONF_VALUE_STRING, list, &error);
	g_free (full);
	g_slist_free (list);
	gconf_extensions_handle_error (&error);
}

GList *
gconf_extensions_get_string_list (const char *prefix, const char *key)
{
	GConfClient *client;
	char *full = NULL;
	GList *result = NULL;
	GSList *list = NULL;
	GSList *iter = NULL;
	GError *error = NULL;

	g_return_val_if_fail (prefix != NULL, NULL);
	g_return_val_if_fail (key != NULL, NULL);

	client = gconf_extensions_client_get ();
	g_return_val_if_fail (client != NULL, NULL);

	full = g_strconcat (prefix, "/", key, NULL);
	list = gconf_client_get_list (client, full, GCONF_VALUE_STRING, &error);
	g_free (full);

	for (iter = list; iter; iter = iter->next) {
		if (iter->data && strlen ((char *)iter->data) > 1) {
			result = g_list_append (result, iter->data);
		}
	}
	g_slist_free (list);
	return result;
}

gboolean
gconf_extensions_handle_error (GError **error)
{
	g_return_val_if_fail (error != NULL, FALSE);

	if (*error != NULL) {
		g_warning ("GConf error:\n  %s", (*error)->message);
		g_error_free (*error);
		*error = NULL;
		return TRUE;
	}
	return FALSE;
}
