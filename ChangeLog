Fri Jan 25 08:43:22 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* gnome-file-selector.c:
	  all pixbuf creation goes through create_pixbuf_scaled (), which scales
	  everything with proper proportions instead of just 20x20.

Thu Jan 17 11:38:52 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* gnome-file-selector-gconf.c:
	  (gconf_extensions_set_string_list) free the GSList when we're done;

Tue Jan 15 10:23:19 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* replace spaces with dashes in history-id keys to make gconf happy.

Thu Jan 10 12:38:32 PM PST 2002 Chris Phelps <chicane@reninet.com>
	* small improvements/tweaks in tab-completion code.

Wed Jan 09 01:56:58 PM PST 2002 Chris Phelps <chicane@reninet.com>
	* fixed to deal with nautilus "default" theme.

Wed Jan 09 10:21:42 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* configure.in:
	  remove libxml dependancy
	* libgnome-file-selector/gnome-file-selector-gconf.c:
	  move all gconf handling code here, implement some new stuff.
	  for list handling.
	* lignome-file-selector/gnome-file-selector-history.c:
	  use gconf instead of custom libxml stuff.

Wed Jan 09 08:23:46 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* libgnome-file-selector/gnome-file-selector.c:
	  UI cleaning by Paolo Maggi <paolo_eli.maggi@katamail.com>
	* Toggle button icon shrinkage for show_dots and show_details

Tue Jan 08 04:56:17 PM PST 2002 Chris Phelps <chicane@reninet.com>
	* libgnome-file-selector/gnome-file-selector.c:
	  (gnome-file-selector-try-complete) initial implementation of tab-completion.

Tue Jan 08 01:59:16 PM PST 2002 Chris Phelps <chicane@reninet.com>
	* libgnome-file-selector/gnome-file-selector.c:
	  implemented mime-type listing and advanced filtering
	  capabilities.
	* libgnome-file-selector/gnome-file-selector-private.h:
	  added GList *mime_types;
	* libgnome-file-selector/gnome-file-selector-factory.c:
	  implement passing of mime-types from moniker stuff.

Tue Jan 08 10:54:17 AM PST 2002 Chris Phelps <chicane@reninet.com>
	* libgnome-file-selector/gnome-file-selector.h:
	  privatized all internals to gnome-file-selector-private.h
	  removed gnome_file_selector_show_dots/details.
	  added gnome_file_selector_set_filename()
	* libgnome-file-selector/gnome-file-selector.c:
	  implemented action and cancel signals
	  added show_dots and show_details as properties
	* libgnome-file-selector/gnome-file-selector-history.c:
	  deal with lack of details/dots accessor functions.
	* libgnome-file-selector/gnome-file-selector-factory.c:
	  use signals, don't access internals
	* src/test-widget.c:
	  don't access internals

2002-01-08  Michael Meeks  <michael@ximian.com>

	* libgnome-file-selector/gnome-file-selector-factory.c
	(get_event_source): split out of
	(action_button_cb): here & fix segv.
	(cancel_button_cb): and here.

2002-01-08  Michael Meeks  <michael@ximian.com>

	* libgnome-file-selector/gnome-file-selector.c
	(insert_node): split out of
	(directory_load_cb): here

	* libgnome-file-selector/gnome-file-selector-history.c
	(check_history_list_length): re-write to be more efficient.
	re-format to a more Gnomeish look.
	(gnome_file_selector_history_add_strings): change semantics.
	(gnome_file_selector_load_history): use atoi not sscanf,
	don't leak XML strings.

	* libgnome-file-selector/gnome-file-selector.c
	(gnome_file_selector_new_with_path): split mostly out into
	(gnome_file_selector_construct): here; impl. for bindings.
	(gnome_file_selector_new_with_path): constify path.
	(get_parent_dir): fix URI reference counting / leak.
	re-format to a more Gnomeish look.
	(delete_file, rename_file): i18n.
	(check_dir_exists, check_file_exists),
	(check_path_local): constify & fix URI leakage.
	(file_selector_setup_default_icons): nail pixbuf leak.

	* libgnome-file-selector/gnome-file-selector.h: re-indent.

Thu Jan 03 07:34:33 PM PST 2002 Chris Phelps <chicane@reninet.com>
    * gnome-file-selector-factory.c: implemented the bonobo file selection stuff.

Sat Dec 29 01:29:18 PM PST 2001 Chris Phelps <chicane@reninet.com>
    * gnome-file-selector-history.c:
      Fixed loading of show-dots and show-details fields.

Sat Dec 29 12:53:45 PM PST 2001 Chris Phelps <chicane@reninet.com>
    * gnome-file-selector.h:
      removed folder_closed_pixbuf member.
      renamed folder_open_pixbuf to folder_pixbuf.
      added gconf_connection for keeping track of out connection to gconf for nautilus folder listening.
      GnomeFileSelectorClass: Fixed typo for GtkDialog -> GtkWindow
    * gnome-file-selector.c:
      Added functions to use nautilus themes for icons in the TreeViews, and
      GConf action for listening to changes (so that changes in the nautilus theme
      will also cause changes in our dialog) At some point in the future, we may want
      to look into doing the toolbar icons in the nautilus style as well.

Thu Dec 13 08:41:00 PM PST 2001 Chris Phelps <chicane@reninet.com>
    * gnome-file-selector.c (gnome_file_selector_get_listing):
      Fixed non-display of files that start with '.'
    * gnome-file-selector.c:
      Implemented DnD support for the directory and file list widgets
      By way of a URI list (drop onto the panel to create a link, etc.
      May make the directory entry the destination for a URI list in
      the future?

Mon Dec 10 02:37:43 AM PST 2001 Chris Phelps <chicane@reninet.com>
    * (gnome-file-selector-history.c) added id based history saving/loading.
